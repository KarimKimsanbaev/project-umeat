﻿Public Class clRecipe
    Public codeRecipe As Long = 0
    Public name As String = "Безымянный"

    'Структура (массив CompositionRecipe) содержит состав рецепта
    Public Structure structCompositionRecipe
        Dim Ingredient As clIngredient 'clIngredient содержит ингредиент
        Dim Md As Double 'Массовая доля ингредиента в рецепте
    End Structure

    Dim cntIng% = 0 'Количество ингредиентов в рецепте
    Public CompositionRecipe() As structCompositionRecipe 'Массив из structCompositionRecipe, содержит состав рецепта, где CompositionRecipe(i) - ингредиент

    'Массив опытных ингредиентов, используется в оптимизации с замещением (не трогать!)
    Dim cntExIng% = 0
    Dim exIngs(5) As structCompositionRecipe




    'Общее содержание основных компонентов
    Public Sбелок# = 0
    Public Sжир# = 0
    Public Sуглеводы# = 0

    Public сухие_вещества = 0
    Public белок# = 0
    Public углеводы# = 0
    Public жиры# = 0
    Public ЭЦобщая# = 0
    Public Sub all()
        белок = 0
        жиры = 0
        углеводы = 0
        сухие_вещества = 0
        ЭЦобщая = 0
        For Each x In CompositionRecipe
            белок += x.Ingredient.components.белок * x.Md
            жиры += x.Ingredient.components.жиры * x.Md
            углеводы += x.Ingredient.components.углеводы * x.Md
            сухие_вещества += x.Ingredient.Содержание_сухих_веществ * x.Md
            ЭЦобщая += x.Ingredient.ЭЦпоумолчанию * x.Md
        Next
    End Sub


    'Энергетическая ценность
    Public ЭЦ_ккал# = 0
    Public ЭЦ_кдж# = 0

    'Биологическая ценность
    Dim КРАС# = 0
    Dim БЦ# = 0
    Dim U# = 0
    Dim G# = 0

    'Цена всего рецепта (может быть где - то считается, но я хз, вроде не делал автоматического расчёта)
    Public Price100g As Double = 0
    Public Price1kg As Double = 0
    Sub SaveToDB()
        WriteRecipeToDB(Me)
    End Sub
    'Конструктор, берёт код, обращается к БД и заполняет clRecipe из БД в соответствии с кодом
    Sub New(code As Long)
        Me.codeRecipe = code
        name = GetNameRecipeByCode(code)
        CompositionRecipe = FillCompositionRecipeByCode(code)
        ЭЦandССВ()
    End Sub
    Public Sub ЭЦandССВ()
        codeRecipe = codeRecipe
        For i = 0 To CompositionRecipe.Length - 1
            CompositionRecipe(i).Ingredient.fillЭЦandССВ()
        Next
    End Sub
    'Пустой конструктор, иниицализирует пустой шаблон clRecipe
    Sub New()
    End Sub
    Public Function OutputDGV(ByRef dgv As DataGridView) As Object


        dgv.Columns.Add("Название_ингредиента", "Рецептурный ингредиент")
        dgv.Columns.Add("Массовая_доля", "Массовая доля в рецепте")
        dgv.Columns.Add("Содержание_сухих_веществ", "Содержание сухих веществ")
        dgv.Columns.Add("Белки", "Белки")
        dgv.Columns.Add("Жиры", "Жиры")
        dgv.Columns.Add("Углеводы", "Углеводы")
        dgv.Columns.Add("ЭЦ", "Энергетическая ценность")
        dgv.Columns.Add("Цена", "Цена")

        For i% = 0 To CompositionRecipe.Length - 1
            Dim row As New DataGridViewRow()
            row.CreateCells(dgv)
            row.Cells(0).Value = CompositionRecipe(i).Ingredient.name
            row.Cells(1).Value = CompositionRecipe(i).Md
            row.Cells(2).Value = CompositionRecipe(i).Ingredient.Содержание_сухих_веществ
            row.Cells(3).Value = CompositionRecipe(i).Ingredient.components.белок
            row.Cells(4).Value = CompositionRecipe(i).Ingredient.components.жиры
            row.Cells(5).Value = CompositionRecipe(i).Ingredient.components.углеводы
            row.Cells(6).Value = CompositionRecipe(i).Ingredient.price

            dgv.Rows.Add(row)
        Next
        dgv.Columns(0).DefaultCellStyle.Font = New System.Drawing.Font("Arial", 10, FontStyle.Bold)
        Return dgv
    End Function
    'Добавляет ингредиент в состав рецепта (ингредиент типа clIngredient, Массовая доля), используется при создании и редактировании рецептов (не трогать)
    Public Sub AddIngredient(ingredient As clIngredient, Optional Md As Double = 0)
        If Not CompositionRecipe Is Nothing Then
            Array.Resize(CompositionRecipe, CompositionRecipe.Length + 1)
        Else
            Array.Resize(CompositionRecipe, 1)
        End If
        CompositionRecipe(CompositionRecipe.Length - 1).Ingredient = ingredient
        CompositionRecipe(CompositionRecipe.Length - 1).Md = Md
    End Sub
    'Добавляет ингредиент по коду, так же как и в варианте выше, только обращается к БД и оттуда автоматически заполняет clIngredient и добавляет в состав рецепта (CompositionRecipe)
    Public Sub AddIngredient(code As Long, Optional Md As Double = 0)
        Dim ingredient As New clIngredient(code)
        If Not CompositionRecipe Is Nothing Then
            Array.Resize(CompositionRecipe, CompositionRecipe.Length + 1)
        Else
            Array.Resize(CompositionRecipe, 1)
        End If
        CompositionRecipe(CompositionRecipe.Length - 1).Ingredient = ingredient
        CompositionRecipe(CompositionRecipe.Length - 1).Md = Md
    End Sub
    'Поменять массовую долю ингредиента в рецепте (порядковый индекс ингредиента и новая массовая доля), как - нибудь заменю, чтобы находил по имени и по коду ингредиента
    Public Sub ChangeMD(indexIngredient%, newMd#)
        CompositionRecipe(indexIngredient).Md = newMd
    End Sub
End Class