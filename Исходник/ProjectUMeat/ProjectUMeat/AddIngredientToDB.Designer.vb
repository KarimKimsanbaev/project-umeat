﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddIngredientToDB
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddIngredientToDB))
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtUglevody = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJir = New System.Windows.Forms.TextBox()
        Me.txtBelok = New System.Windows.Forms.TextBox()
        Me.txtWater = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtLizin = New System.Windows.Forms.TextBox()
        Me.txtTreon = New System.Windows.Forms.TextBox()
        Me.txtTriptofan = New System.Windows.Forms.TextBox()
        Me.txtFen_Tir = New System.Windows.Forms.TextBox()
        Me.txtMet_Cist = New System.Windows.Forms.TextBox()
        Me.txtValin = New System.Windows.Forms.TextBox()
        Me.txtLei = New System.Windows.Forms.TextBox()
        Me.txtIzo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnOptim = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(20, 179)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 19)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Углеводы:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(20, 132)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 19)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Жиры:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(20, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 19)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Белки:"
        '
        'txtUglevody
        '
        Me.txtUglevody.Location = New System.Drawing.Point(151, 176)
        Me.txtUglevody.Name = "txtUglevody"
        Me.txtUglevody.Size = New System.Drawing.Size(100, 27)
        Me.txtUglevody.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(20, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 19)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Вода:"
        '
        'txtJir
        '
        Me.txtJir.Location = New System.Drawing.Point(151, 129)
        Me.txtJir.Name = "txtJir"
        Me.txtJir.Size = New System.Drawing.Size(100, 27)
        Me.txtJir.TabIndex = 4
        '
        'txtBelok
        '
        Me.txtBelok.Location = New System.Drawing.Point(151, 85)
        Me.txtBelok.Name = "txtBelok"
        Me.txtBelok.Size = New System.Drawing.Size(100, 27)
        Me.txtBelok.TabIndex = 3
        '
        'txtWater
        '
        Me.txtWater.Location = New System.Drawing.Point(151, 41)
        Me.txtWater.Name = "txtWater"
        Me.txtWater.Size = New System.Drawing.Size(100, 27)
        Me.txtWater.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(58, 237)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(60, 19)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Лизин:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(58, 211)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(60, 19)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "Треон:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(58, 183)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 19)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Триптофан:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(58, 157)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(91, 19)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Фен + Тир:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(58, 132)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 19)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Мет + Цист:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(58, 104)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 19)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Валин:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(58, 78)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 19)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Лейцин:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(58, 52)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 19)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Изолецин:"
        '
        'txtLizin
        '
        Me.txtLizin.Location = New System.Drawing.Point(181, 232)
        Me.txtLizin.Name = "txtLizin"
        Me.txtLizin.Size = New System.Drawing.Size(100, 27)
        Me.txtLizin.TabIndex = 10
        '
        'txtTreon
        '
        Me.txtTreon.Location = New System.Drawing.Point(181, 206)
        Me.txtTreon.Name = "txtTreon"
        Me.txtTreon.Size = New System.Drawing.Size(100, 27)
        Me.txtTreon.TabIndex = 9
        '
        'txtTriptofan
        '
        Me.txtTriptofan.Location = New System.Drawing.Point(181, 180)
        Me.txtTriptofan.Name = "txtTriptofan"
        Me.txtTriptofan.Size = New System.Drawing.Size(100, 27)
        Me.txtTriptofan.TabIndex = 8
        '
        'txtFen_Tir
        '
        Me.txtFen_Tir.Location = New System.Drawing.Point(181, 154)
        Me.txtFen_Tir.Name = "txtFen_Tir"
        Me.txtFen_Tir.Size = New System.Drawing.Size(100, 27)
        Me.txtFen_Tir.TabIndex = 7
        '
        'txtMet_Cist
        '
        Me.txtMet_Cist.AcceptsReturn = True
        Me.txtMet_Cist.Location = New System.Drawing.Point(181, 127)
        Me.txtMet_Cist.Name = "txtMet_Cist"
        Me.txtMet_Cist.Size = New System.Drawing.Size(100, 27)
        Me.txtMet_Cist.TabIndex = 5
        '
        'txtValin
        '
        Me.txtValin.Location = New System.Drawing.Point(181, 101)
        Me.txtValin.Name = "txtValin"
        Me.txtValin.Size = New System.Drawing.Size(100, 27)
        Me.txtValin.TabIndex = 5
        '
        'txtLei
        '
        Me.txtLei.Location = New System.Drawing.Point(181, 75)
        Me.txtLei.Name = "txtLei"
        Me.txtLei.Size = New System.Drawing.Size(100, 27)
        Me.txtLei.TabIndex = 5
        '
        'txtIzo
        '
        Me.txtIzo.Location = New System.Drawing.Point(181, 49)
        Me.txtIzo.Name = "txtIzo"
        Me.txtIzo.Size = New System.Drawing.Size(100, 27)
        Me.txtIzo.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(54, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Цена:"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtName.Location = New System.Drawing.Point(225, 12)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(369, 29)
        Me.txtName.TabIndex = 1
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(204, 54)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 27)
        Me.TextBox6.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(12, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(204, 18)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Название ингредиента:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.txtWater)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.txtBelok)
        Me.GroupBox4.Controls.Add(Me.txtUglevody)
        Me.GroupBox4.Controls.Add(Me.txtJir)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Location = New System.Drawing.Point(12, 64)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(313, 257)
        Me.GroupBox4.TabIndex = 22
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Содержание основных компонентов"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.txtIzo)
        Me.GroupBox5.Controls.Add(Me.Label12)
        Me.GroupBox5.Controls.Add(Me.txtLei)
        Me.GroupBox5.Controls.Add(Me.Label11)
        Me.GroupBox5.Controls.Add(Me.txtValin)
        Me.GroupBox5.Controls.Add(Me.Label10)
        Me.GroupBox5.Controls.Add(Me.txtMet_Cist)
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Controls.Add(Me.txtFen_Tir)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.txtTriptofan)
        Me.GroupBox5.Controls.Add(Me.txtTreon)
        Me.GroupBox5.Controls.Add(Me.txtLizin)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox5.Location = New System.Drawing.Point(350, 64)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(360, 318)
        Me.GroupBox5.TabIndex = 23
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Содержание Аминокислот"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextBox1)
        Me.GroupBox6.Controls.Add(Me.Label15)
        Me.GroupBox6.Controls.Add(Me.TextBox6)
        Me.GroupBox6.Controls.Add(Me.Label1)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox6.Location = New System.Drawing.Point(350, 398)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(360, 175)
        Me.GroupBox6.TabIndex = 24
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Общие показатели рецептуры"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(204, 112)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 27)
        Me.TextBox1.TabIndex = 7
        '
        'Label15
        '
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(14, 101)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(152, 47)
        Me.Label15.TabIndex = 6
        Me.Label15.Text = "Энергетическая ценность"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnOptim
        '
        Me.btnOptim.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.btnOptim.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnOptim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnOptim.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatAppearance.BorderSize = 0
        Me.btnOptim.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.btnOptim.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOptim.Font = New System.Drawing.Font("Microsoft YaHei UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnOptim.ForeColor = System.Drawing.Color.White
        Me.btnOptim.Image = CType(resources.GetObject("btnOptim.Image"), System.Drawing.Image)
        Me.btnOptim.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOptim.Location = New System.Drawing.Point(27, 423)
        Me.btnOptim.Name = "btnOptim"
        Me.btnOptim.Size = New System.Drawing.Size(176, 56)
        Me.btnOptim.TabIndex = 25
        Me.btnOptim.Text = "Добавить"
        Me.btnOptim.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Button3.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.Button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft YaHei UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(27, 485)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(176, 56)
        Me.Button3.TabIndex = 26
        Me.Button3.Text = "Отмена"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'AddIngredientToDB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(750, 583)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnOptim)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtName)
        Me.Name = "AddIngredientToDB"
        Me.Text = "Добавление ингредиента"
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJir As TextBox
    Friend WithEvents txtBelok As TextBox
    Friend WithEvents txtWater As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtMet_Cist As TextBox
    Friend WithEvents txtValin As TextBox
    Friend WithEvents txtLei As TextBox
    Friend WithEvents txtIzo As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtUglevody As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtLizin As TextBox
    Friend WithEvents txtTreon As TextBox
    Friend WithEvents txtTriptofan As TextBox
    Friend WithEvents txtFen_Tir As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents btnOptim As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label15 As Label
End Class
