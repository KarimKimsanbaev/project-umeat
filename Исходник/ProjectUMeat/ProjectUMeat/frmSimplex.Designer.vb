﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSimplex
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvA = New System.Windows.Forms.DataGridView()
        Me.dgvNotBazis = New System.Windows.Forms.DataGridView()
        Me.dgvBazis = New System.Windows.Forms.DataGridView()
        Me.dgvB = New System.Windows.Forms.DataGridView()
        Me.dgvF = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ValueTargetF = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtLog = New System.Windows.Forms.RichTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgvMin = New System.Windows.Forms.DataGridView()
        Me.dgvX = New System.Windows.Forms.DataGridView()
        CType(Me.dgvA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNotBazis, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBazis, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvX, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvA
        '
        Me.dgvA.AllowUserToAddRows = False
        Me.dgvA.AllowUserToDeleteRows = False
        Me.dgvA.AllowUserToResizeColumns = False
        Me.dgvA.AllowUserToResizeRows = False
        Me.dgvA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvA.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvA.ColumnHeadersVisible = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvA.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvA.Location = New System.Drawing.Point(152, 98)
        Me.dgvA.Name = "dgvA"
        Me.dgvA.ReadOnly = True
        Me.dgvA.RowHeadersVisible = False
        Me.dgvA.Size = New System.Drawing.Size(240, 150)
        Me.dgvA.TabIndex = 0
        '
        'dgvNotBazis
        '
        Me.dgvNotBazis.AllowUserToAddRows = False
        Me.dgvNotBazis.AllowUserToDeleteRows = False
        Me.dgvNotBazis.AllowUserToResizeColumns = False
        Me.dgvNotBazis.AllowUserToResizeRows = False
        Me.dgvNotBazis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvNotBazis.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvNotBazis.ColumnHeadersVisible = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvNotBazis.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvNotBazis.Location = New System.Drawing.Point(152, 56)
        Me.dgvNotBazis.Name = "dgvNotBazis"
        Me.dgvNotBazis.ReadOnly = True
        Me.dgvNotBazis.RowHeadersVisible = False
        Me.dgvNotBazis.RowHeadersWidth = 50
        Me.dgvNotBazis.Size = New System.Drawing.Size(240, 36)
        Me.dgvNotBazis.TabIndex = 1
        '
        'dgvBazis
        '
        Me.dgvBazis.AllowUserToAddRows = False
        Me.dgvBazis.AllowUserToDeleteRows = False
        Me.dgvBazis.AllowUserToResizeColumns = False
        Me.dgvBazis.AllowUserToResizeRows = False
        Me.dgvBazis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvBazis.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvBazis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBazis.ColumnHeadersVisible = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBazis.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvBazis.Location = New System.Drawing.Point(104, 98)
        Me.dgvBazis.Name = "dgvBazis"
        Me.dgvBazis.ReadOnly = True
        Me.dgvBazis.RowHeadersVisible = False
        Me.dgvBazis.Size = New System.Drawing.Size(42, 150)
        Me.dgvBazis.TabIndex = 2
        '
        'dgvB
        '
        Me.dgvB.AllowUserToAddRows = False
        Me.dgvB.AllowUserToDeleteRows = False
        Me.dgvB.AllowUserToResizeColumns = False
        Me.dgvB.AllowUserToResizeRows = False
        Me.dgvB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvB.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvB.ColumnHeadersVisible = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvB.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvB.Location = New System.Drawing.Point(398, 98)
        Me.dgvB.Name = "dgvB"
        Me.dgvB.ReadOnly = True
        Me.dgvB.RowHeadersVisible = False
        Me.dgvB.RowHeadersWidth = 100
        Me.dgvB.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvB.Size = New System.Drawing.Size(66, 150)
        Me.dgvB.TabIndex = 3
        '
        'dgvF
        '
        Me.dgvF.AllowUserToAddRows = False
        Me.dgvF.AllowUserToDeleteRows = False
        Me.dgvF.AllowUserToResizeColumns = False
        Me.dgvF.AllowUserToResizeRows = False
        Me.dgvF.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvF.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvF.ColumnHeadersVisible = False
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvF.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvF.Location = New System.Drawing.Point(152, 254)
        Me.dgvF.Name = "dgvF"
        Me.dgvF.ReadOnly = True
        Me.dgvF.RowHeadersVisible = False
        Me.dgvF.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvF.Size = New System.Drawing.Size(240, 36)
        Me.dgvF.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Vivaldi", 26.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(121, 248)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 42)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "f"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Vivaldi", 26.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(398, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 42)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "b"
        '
        'ValueTargetF
        '
        Me.ValueTargetF.Font = New System.Drawing.Font("Mistral", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ValueTargetF.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ValueTargetF.Location = New System.Drawing.Point(399, 255)
        Me.ValueTargetF.Multiline = True
        Me.ValueTargetF.Name = "ValueTargetF"
        Me.ValueTargetF.Size = New System.Drawing.Size(41, 35)
        Me.ValueTargetF.TabIndex = 7
        Me.ValueTargetF.Text = "123"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Vivaldi", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(193, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(163, 29)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Не базисные"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Vivaldi", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte), True)
        Me.Label4.Location = New System.Drawing.Point(19, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(127, 29)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Базисные"
        '
        'txtLog
        '
        Me.txtLog.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtLog.Location = New System.Drawing.Point(662, 56)
        Me.txtLog.Name = "txtLog"
        Me.txtLog.Size = New System.Drawing.Size(480, 349)
        Me.txtLog.TabIndex = 10
        Me.txtLog.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Vivaldi", 16.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label5.Location = New System.Drawing.Point(830, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 26)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Журнал"
        '
        'dgvMin
        '
        Me.dgvMin.AllowUserToAddRows = False
        Me.dgvMin.AllowUserToDeleteRows = False
        Me.dgvMin.AllowUserToResizeColumns = False
        Me.dgvMin.AllowUserToResizeRows = False
        Me.dgvMin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvMin.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvMin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMin.ColumnHeadersVisible = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMin.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvMin.Location = New System.Drawing.Point(506, 98)
        Me.dgvMin.Name = "dgvMin"
        Me.dgvMin.ReadOnly = True
        Me.dgvMin.RowHeadersVisible = False
        Me.dgvMin.RowHeadersWidth = 100
        Me.dgvMin.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMin.Size = New System.Drawing.Size(42, 150)
        Me.dgvMin.TabIndex = 12
        '
        'dgvX
        '
        Me.dgvX.AllowUserToAddRows = False
        Me.dgvX.AllowUserToDeleteRows = False
        Me.dgvX.AllowUserToResizeColumns = False
        Me.dgvX.AllowUserToResizeRows = False
        Me.dgvX.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvX.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvX.ColumnHeadersVisible = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvX.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvX.Location = New System.Drawing.Point(584, 12)
        Me.dgvX.Name = "dgvX"
        Me.dgvX.ReadOnly = True
        Me.dgvX.RowHeadersVisible = False
        Me.dgvX.RowHeadersWidth = 50
        Me.dgvX.Size = New System.Drawing.Size(240, 36)
        Me.dgvX.TabIndex = 13
        '
        'frmSimplex
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1154, 420)
        Me.Controls.Add(Me.dgvX)
        Me.Controls.Add(Me.dgvMin)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtLog)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ValueTargetF)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvF)
        Me.Controls.Add(Me.dgvB)
        Me.Controls.Add(Me.dgvBazis)
        Me.Controls.Add(Me.dgvNotBazis)
        Me.Controls.Add(Me.dgvA)
        Me.Name = "frmSimplex"
        Me.Text = "Simplex"
        CType(Me.dgvA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNotBazis, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBazis, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvX, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvA As DataGridView
    Friend WithEvents dgvNotBazis As DataGridView
    Friend WithEvents dgvBazis As DataGridView
    Friend WithEvents dgvB As DataGridView
    Friend WithEvents dgvF As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ValueTargetF As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtLog As RichTextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents dgvMin As DataGridView
    Friend WithEvents dgvX As DataGridView
End Class
