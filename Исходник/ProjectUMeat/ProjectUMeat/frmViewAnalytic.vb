﻿Imports System.Data.SqlClient
Public Class frmViewAnalytic
    Public code% = 10047
    Private recipe As clRecipe
    Private Sub frmViewAnalytic_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WindowState = FormWindowState.Maximized
        If code = 0 Then
            'MsgBox("Не задан рецепт")
            Dim dt As New DataTable
            'dt = frmOptim2.recipe.GetDataRecipe()
            'DataGridView1.DataSource = dt
            'DataGridView1.Columns(0).Visible = False
            Return
        End If
        Try
            Me.ViewReceptTableAdapter.FillBy1(Me.ProductsDBDataSet.ViewRecept, CType(code, Long))
        Catch ex As Exception
        End Try

        Dim nameRecipe As String = ""

        nameRecipe = Me.RecipesTableAdapter1.GetNameRecipe(code)
        Label1.Text = nameRecipe

        advanced()
        Y()
    End Sub
    Private Sub advanced()
        recipe = New clRecipe(code)
        Dim dgvView As New ViewRecipeOnDGV(DataGridView1, recipe)

        dgvБелковыйКомпонент.DefaultCellStyle.Format = "0.###"
        dgvБелковыйКомпонент.DefaultCellStyle.SelectionBackColor = Color.White
        dgvБелковыйКомпонент.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvНак_Скор.DefaultCellStyle.Format = "0.###"
        dgvНак_Скор.DefaultCellStyle.SelectionBackColor = Color.White
        dgvНак_Скор.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvОсновныеКомпоненты.DefaultCellStyle.Format = "0.###"
        dgvОсновныеКомпоненты.DefaultCellStyle.SelectionBackColor = Color.White
        dgvОсновныеКомпоненты.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvЭнергетическаяЦенность.DefaultCellStyle.Format = "0.###"
        dgvЭнергетическаяЦенность.DefaultCellStyle.SelectionBackColor = Color.White
        dgvЭнергетическаяЦенность.DefaultCellStyle.SelectionForeColor = Color.Black

        DataGridView1.DefaultCellStyle.Format = "0.###"
        DataGridView1.DefaultCellStyle.SelectionBackColor = Color.White
        DataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black

        DataGridView1.DefaultCellStyle.Alignment = ContentAlignment.MiddleCenter


    End Sub
    Private Sub Y()
        ЭЦ(recipe)

        Dim row As New DataGridViewRow
        row.CreateCells(dgvОсновныеКомпоненты)

        row.Cells(0).Value = recipe.Sбелок
        row.Cells(1).Value = recipe.Sжир
        row.Cells(2).Value = recipe.Sуглеводы

        dgvОсновныеКомпоненты.Rows.Add(row)

        Dim row2 As New DataGridViewRow
        row2.CreateCells(dgvЭнергетическаяЦенность)
        row2.Cells(0).Value = recipe.ЭЦ_ккал
        row2.Cells(1).Value = recipe.ЭЦ_кдж
        dgvЭнергетическаяЦенность.Rows.Add(row2)

        Dim row3 As New DataGridViewRow
        row3.CreateCells(dgvНак_Скор)
        row3.Cells(0).Value = "М, %"
        row3.Cells(0).Style.BackColor = Color.Aqua
        For i% = 1 To 8
            row3.Cells(i).Value = Аминокислоты_М(i, recipe)
        Next
        dgvНак_Скор.Rows.Add(row3)

        Dim row4 As New DataGridViewRow
        row4.CreateCells(dgvНак_Скор)
        row4.Cells(0).Value = "С, %"
        row4.Cells(0).Style.BackColor = Color.Pink
        For i% = 1 To dgvНак_Скор.ColumnCount - 1
        Next
        row4.Cells(1).Value = row3.Cells(1).Value / 4 * 100
        row4.Cells(2).Value = row3.Cells(2).Value / 7 * 100
        row4.Cells(3).Value = row3.Cells(3).Value / 5 * 100
        row4.Cells(4).Value = row3.Cells(4).Value / 3.5 * 100
        row4.Cells(5).Value = row3.Cells(5).Value / 6 * 100
        row4.Cells(6).Value = row3.Cells(6).Value / 1 * 100
        row4.Cells(7).Value = row3.Cells(7).Value / 5.5 * 100
        row4.Cells(8).Value = row3.Cells(8).Value / 4 * 100
        dgvНак_Скор.Rows.Add(row4)

        Dim Cmin As Double
        Cmin = Расчёт_Cmin(row4)
        txtCmin.Text = Cmin

        Dim row5 As New DataGridViewRow
        row5.CreateCells(dgvНак_Скор)
        row5.Cells(0).Value = "a, %"
        row5.Cells(0).Style.BackColor = Color.Yellow
        For i% = 1 To dgvНак_Скор.ColumnCount - 1
            row5.Cells(i).Value = Cmin / row4.Cells(i).Value
        Next
        dgvНак_Скор.Rows.Add(row5)

        Dim row6 As New DataGridViewRow
        row6.CreateCells(dgvБелковыйКомпонент)
        row6.Cells(0).Value = Расчёт_КРАС(row4, recipe)
        row6.Cells(1).Value = 100 - row6.Cells(0).Value
        row6.Cells(2).Value = Расчёт_U(row3, row5, recipe)
        row6.Cells(3).Value = Расчёт_G(row3, row5, recipe)
        dgvБелковыйКомпонент.Rows.Add(row6)

        chrБелковыйПоказатель.Series.Add("КРАС")
        chrБелковыйПоказатель.Series("КРАС").Points.AddXY("КРАС", row6.Cells(0).Value)
        chrБелковыйПоказатель.Series.Add("БЦ")
        chrБелковыйПоказатель.Series("БЦ").Points.AddXY("БЦ", row6.Cells(1).Value)
        chrБелковыйПоказатель.Series.Add("U")
        chrБелковыйПоказатель.Series("U").Points.AddXY("U", row6.Cells(2).Value)
        chrБелковыйПоказатель.Series.Add("G")
        chrБелковыйПоказатель.Series("G").Points.AddXY("G", row6.Cells(3).Value)
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Y()
    End Sub
End Class