﻿Module Функции__которые_забыли_в_Майкрософте
    Public Function GetTemplateTable(row As DataRow) As DataTable
        Dim table As New DataTable
        Dim colColumn As DataColumnCollection
        colColumn = row.Table.Columns

        Dim str As String
        For i = 0 To colColumn.Count - 1
            str = colColumn.Item(i).ColumnName

            'Как - то оптимизировать этот кусок!

            table.Columns.Add(str)

        Next

        table.Columns(1).Unique = True

        Return table
    End Function
    'Возвращает истину, если произошла ошибка. Запилить сюда Try...Catch
    Public Function AddRow(row As DataRow, ByRef table As DataTable) As Boolean
        If table.Columns.Count = 0 Then
            table = GetTemplateTable(row)
            Debug.WriteLine("Создаём шаблон из БД для таблицы")
        End If

        Try
            table.ImportRow(row)
        Catch ex As ConstraintException
            MsgBox("Такой ингредиент уже существует в рецептуре")
        End Try
        Return False
    End Function
    'Function word(dg As DataGridView)
    '    Dim app As New Microsoft.Office.Interop.Word.Application
    '    Dim appdoc As New Microsoft.Office.Interop.Word.Document

    '    app.Visible = False
    '    '  appdoc.Application.ad
    '    appdoc = app.Documents.Add("C:\test.docx")
    '    appdoc.PageSetup.Orientation = Microsoft.Office.Interop.Word.WdOrientation.wdOrientLandscape
    '    Dim wordTable As Microsoft.Office.Interop.Word.Table
    '    With appdoc
    '        Dim r, c As Integer
    '        r = dg.Rows.Count + 1
    '        c = dg.Columns.Count
    '        wordTable = .Tables.Add(app.Selection.Range, r, c)
    '        For i = 0 To wordTable.Borders.Count
    '            wordTable.Borders.Item(Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom).LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle
    '            wordTable.Borders.Item(Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop).LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle
    '            wordTable.Borders.Item(Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft).LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle
    '            wordTable.Borders.Item(Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight).LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle
    '            wordTable.Borders.Item(Microsoft.Office.Interop.Word.WdBorderType.wdBorderHorizontal).LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle
    '            wordTable.Borders.Item(Microsoft.Office.Interop.Word.WdBorderType.wdBorderVertical).LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle

    '        Next
    '    End With
    '    wordTable.Rows(1).Select()
    '    app.Selection.Font.Bold = 1
    '    For i = 0 To dg.ColumnCount - 1
    '        If dg.Columns(i).Visible = True Then

    '            wordTable.Cell(1, i + 1).Range.Text = dg.Columns(i).HeaderText
    '        End If
    '    Next
    '    For i = 1 To dg.Rows.Count
    '        For j = 1 To dg.ColumnCount
    '            If dg.Columns(j - 1).Visible = True Then
    '                wordTable.Cell(i + 1, j).Range.Text = dg.Rows(i - 1).Cells(j - 1).Value.ToString
    '                wordTable.Cell(i + 1, j).WordWrap = True
    '            End If

    '        Next
    '    Next

    '    wordTable.ID = "Report"

    '    wordTable.PreferredWidthType = Microsoft.Office.Interop.Word.WdPreferredWidthType.wdPreferredWidthAuto

    '    wordTable.Select()
    '    app.Selection.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd)
    '    app.Visible = True
    'End Function
End Module
