﻿Class ViewRecipeOnDGV
    Private dgvViewRecipe As DataGridView
    Private recipe As clRecipe

    Sub New(dgv As DataGridView, recipe As clRecipe)
        dgvViewRecipe = dgv
        Me.recipe = recipe

        OptionsDGV()
        FillColumns()
        UpdateDGV()
    End Sub
    Private Sub OptionsDGV()
        dgvViewRecipe.AllowUserToAddRows = False
        dgvViewRecipe.AllowUserToDeleteRows = False
    End Sub
    Private Sub FillColumns()
        Dim colColumn As DataGridViewColumnCollection
        colColumn = dgvViewRecipe.Columns

        AddColumnsAdvanced(colColumn)
        AddColumnsMainComponents(colColumn)
        AddColumnsAminokisloty(colColumn)
    End Sub
    Private Sub AddColumnsAdvanced(colColumn As DataGridViewColumnCollection)
        AddColumn(colColumn, "Название")
        AddColumn(colColumn, "Массовая доля")
        AddColumn(colColumn, "Цена за 1 кг")
    End Sub
    Private Sub AddColumnsMainComponents(colColumn As DataGridViewColumnCollection)
        AddColumn(colColumn, "Белки")
        AddColumn(colColumn, "Жиры")
        AddColumn(colColumn, "Углеводы")
    End Sub
    Private Sub AddColumnsAminokisloty(colColumn As DataGridViewColumnCollection)
        AddColumn(colColumn, "Изолейцин")
        AddColumn(colColumn, "Лейцин")
        AddColumn(colColumn, "Валин")
        AddColumn(colColumn, "Мет+Цист")
        AddColumn(colColumn, "Фен+Тир")
        AddColumn(colColumn, "Триптофан")
        AddColumn(colColumn, "Лизин")
        AddColumn(colColumn, "Треон")
    End Sub
    Private Sub AddColumn(colColumn As DataGridViewColumnCollection, name As String)
        'Как - то это всё сомнительно, закину как я всё это в Try Catch
        Try
            colColumn.Add(name, name)
        Catch ex As Exception
            MessageBox.Show("Неовзомжно задать имя для столбца: " + name)
        End Try
    End Sub
    Private Sub UpdateDGV()
        RemoveAllRows()
        AddRowsFromRecipe()
    End Sub
    Private Sub RemoveAllRows()
        While dgvViewRecipe.Rows.Count <> 0
            If Not dgvViewRecipe.Rows(0).IsNewRow Then
                dgvViewRecipe.Rows.Remove(dgvViewRecipe.Rows(0))
            End If
        End While
    End Sub
    Private Sub AddRowsFromRecipe()
        Dim row As DataGridViewRow
        Dim index As Integer

        For i% = 0 To recipe.CompositionRecipe.Length - 1
            index = dgvViewRecipe.Rows.Add()
            row = dgvViewRecipe.Rows(index)
            ViewAdvanced(row, i)
            ViewMainComponents(row, i)
            ViewAminokisloty(row, i)
        Next
    End Sub
    Private Sub ViewAdvanced(row As DataGridViewRow, ind%)
        row.Cells("Название").Value = recipe.CompositionRecipe(ind).Ingredient.name
        row.Cells("Массовая доля").Value = recipe.CompositionRecipe(ind).Md
    End Sub
    Private Sub ViewMainComponents(row As DataGridViewRow, ind%)
        row.Cells("Белки").Value = recipe.CompositionRecipe(ind).Ingredient.components.белок
        row.Cells("Жиры").Value = recipe.CompositionRecipe(ind).Ingredient.components.жиры
        row.Cells("Углеводы").Value = recipe.CompositionRecipe(ind).Ingredient.components.углеводы
    End Sub
    Private Sub ViewAminokisloty(row As DataGridViewRow, ind%)
        row.Cells("Изолейцин").Value = recipe.CompositionRecipe(ind).Ingredient.components.изолейц
        row.Cells("Лейцин").Value = recipe.CompositionRecipe(ind).Ingredient.components.лейц
        row.Cells("Валин").Value = recipe.CompositionRecipe(ind).Ingredient.components.валин
        row.Cells("Мет+Цист").Value = recipe.CompositionRecipe(ind).Ingredient.components.мет_цист
        row.Cells("Фен+Тир").Value = recipe.CompositionRecipe(ind).Ingredient.components.фен_тир
        row.Cells("Триптофан").Value = recipe.CompositionRecipe(ind).Ingredient.components.триптофан
        row.Cells("Лизин").Value = recipe.CompositionRecipe(ind).Ingredient.components.лизин
        row.Cells("Треон").Value = recipe.CompositionRecipe(ind).Ingredient.components.треон
    End Sub
End Class