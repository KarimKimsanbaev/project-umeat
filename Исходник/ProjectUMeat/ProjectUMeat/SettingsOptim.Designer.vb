﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SettingsOptim
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvViewSettings = New System.Windows.Forms.DataGridView()
        Me.Название_компонента = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.МД_Контрольного_образца = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvZamSettings = New System.Windows.Forms.DataGridView()
        Me.Опытный_компонент = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TextColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Замещаемый_компонент = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.dgvViewSettings, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvZamSettings, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvViewSettings
        '
        Me.dgvViewSettings.AllowUserToAddRows = False
        Me.dgvViewSettings.AllowUserToDeleteRows = False
        Me.dgvViewSettings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvViewSettings.BackgroundColor = System.Drawing.Color.Linen
        Me.dgvViewSettings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvViewSettings.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvViewSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvViewSettings.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Название_компонента, Me.МД_Контрольного_образца})
        Me.dgvViewSettings.Location = New System.Drawing.Point(471, 36)
        Me.dgvViewSettings.Name = "dgvViewSettings"
        Me.dgvViewSettings.RowHeadersVisible = False
        Me.dgvViewSettings.Size = New System.Drawing.Size(652, 352)
        Me.dgvViewSettings.TabIndex = 0
        '
        'Название_компонента
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Название_компонента.DefaultCellStyle = DataGridViewCellStyle2
        Me.Название_компонента.HeaderText = "Название компонента"
        Me.Название_компонента.Name = "Название_компонента"
        Me.Название_компонента.Width = 133
        '
        'МД_Контрольного_образца
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.МД_Контрольного_образца.DefaultCellStyle = DataGridViewCellStyle3
        Me.МД_Контрольного_образца.HeaderText = "Контрольный образец"
        Me.МД_Контрольного_образца.Name = "МД_Контрольного_образца"
        Me.МД_Контрольного_образца.Width = 132
        '
        'dgvZamSettings
        '
        Me.dgvZamSettings.AllowUserToAddRows = False
        Me.dgvZamSettings.AllowUserToDeleteRows = False
        Me.dgvZamSettings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvZamSettings.BackgroundColor = System.Drawing.Color.Linen
        Me.dgvZamSettings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvZamSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvZamSettings.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Опытный_компонент, Me.TextColumn, Me.Замещаемый_компонент})
        Me.dgvZamSettings.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvZamSettings.Location = New System.Drawing.Point(12, 36)
        Me.dgvZamSettings.Name = "dgvZamSettings"
        Me.dgvZamSettings.RowHeadersVisible = False
        Me.dgvZamSettings.Size = New System.Drawing.Size(423, 230)
        Me.dgvZamSettings.TabIndex = 1
        '
        'Опытный_компонент
        '
        Me.Опытный_компонент.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Опытный_компонент.HeaderText = "Опытный компонент"
        Me.Опытный_компонент.Name = "Опытный_компонент"
        Me.Опытный_компонент.ReadOnly = True
        Me.Опытный_компонент.Width = 200
        '
        'TextColumn
        '
        Me.TextColumn.HeaderText = ""
        Me.TextColumn.Name = "TextColumn"
        Me.TextColumn.ReadOnly = True
        Me.TextColumn.Width = 19
        '
        'Замещаемый_компонент
        '
        Me.Замещаемый_компонент.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Замещаемый_компонент.HeaderText = "Замещаемый компонент"
        Me.Замещаемый_компонент.Name = "Замещаемый_компонент"
        Me.Замещаемый_компонент.Width = 200
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(100, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(210, 24)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Настройка замещения"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(247, 272)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(188, 116)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Расчитать"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.ProjectUMeat.My.Resources.Resources.file_ad
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.Location = New System.Drawing.Point(12, 272)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(229, 55)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Добавить Компонент"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.ProjectUMeat.My.Resources.Resources.big_news_dealer_170555
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.Location = New System.Drawing.Point(12, 333)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(229, 55)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Добавить опытный образец"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'SettingsOptim
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Bisque
        Me.ClientSize = New System.Drawing.Size(1143, 403)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dgvZamSettings)
        Me.Controls.Add(Me.dgvViewSettings)
        Me.Name = "SettingsOptim"
        Me.Text = "Оптимизация с замещением"
        CType(Me.dgvViewSettings, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvZamSettings, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvViewSettings As DataGridView
    Friend WithEvents dgvZamSettings As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Название_компонента As DataGridViewTextBoxColumn
    Friend WithEvents МД_Контрольного_образца As DataGridViewTextBoxColumn
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Опытный_компонент As DataGridViewTextBoxColumn
    Friend WithEvents TextColumn As DataGridViewTextBoxColumn
    Friend WithEvents Замещаемый_компонент As DataGridViewComboBoxColumn
End Class
