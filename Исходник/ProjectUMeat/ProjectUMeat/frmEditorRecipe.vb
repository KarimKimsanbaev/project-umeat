﻿Public Class frmEditorRecipe
    Public code% = 0
    Private recipe As clRecipe

    Private Sub frmEditorRecipe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "ProductsDBDataSet.Ingredients". При необходимости она может быть перемещена или удалена.
        Me.IngredientsTableAdapter.Fill(Me.ProductsDBDataSet.Ingredients)
        CodeRecipe.Text = code
        If code <> 0 Then
            Try
                recipe = New clRecipe(code)
            Catch ex As Exception
                MsgBox("Такого рецепта нет в базе данных или база данных не подключена")
            End Try
            txtNameRecipe.Text = recipe.name
            fillDGV(recipe)
        Else
            recipe = New clRecipe()
        End If
    End Sub
    Private Sub fillDGV(recipe As clRecipe)
        Try
            For Each x As clRecipe.structCompositionRecipe In recipe.CompositionRecipe
                Dim row As DataGridViewRow
                Dim ind% = dgvRecipe1.Rows.Add()
                Debug.WriteLine(ind)
                row = dgvRecipe1.Rows(ind)
                'row.CreateCells(dgvRecipe1)
                row.Cells("Название_ингредиента").Value = x.Ingredient.name
                row.Cells("Массовая_доля").Value = x.Md
                row.Cells("Белки").Value = x.Ingredient.components.белок
                row.Cells("Жиры").Value = x.Ingredient.components.жиры
                row.Cells("Углеводы").Value = x.Ingredient.components.углеводы
                row.Cells("Цена").Value = x.Ingredient.price
                row.Cells("ЭЦ").Value = x.Ingredient.ЭЦпоумолчанию

                'dgvRecipe1.Rows.Add(row)
            Next
        Catch ex As Exception
            MsgBox("Ошибка связи с сервером")
            Application.Exit()
        End Try

        fillDGVОсновныеКомпоненты()
        fillЭЦ()
        fillСкор()
        fillБелковыйКомпонент()

        dgvБелковыйКомпонент.DefaultCellStyle.Format = "0.###"
        dgvБелковыйКомпонент.DefaultCellStyle.SelectionBackColor = Color.White
        dgvБелковыйКомпонент.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvНак_Скор.DefaultCellStyle.Format = "0.###"
        dgvНак_Скор.SelectionMode = Nothing
        dgvНак_Скор.DefaultCellStyle.SelectionBackColor = Color.White
        dgvНак_Скор.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvОсновныеКомпоненты.DefaultCellStyle.Format = "0.###"
        dgvОсновныеКомпоненты.DefaultCellStyle.SelectionBackColor = Color.White
        dgvОсновныеКомпоненты.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvЭнергетическаяЦенность.DefaultCellStyle.Format = "0.###"
        dgvЭнергетическаяЦенность.DefaultCellStyle.SelectionBackColor = Color.White
        dgvЭнергетическаяЦенность.DefaultCellStyle.SelectionForeColor = Color.Black

        dgvRecipe1.DefaultCellStyle.Format = "0.###"
        dgvRecipe1.DefaultCellStyle.SelectionBackColor = Color.White
        dgvRecipe1.DefaultCellStyle.SelectionForeColor = Color.Black

        'DataGridView1.DefaultCellStyle.Alignment = ContentAlignment.MiddleCenter
    End Sub
    Sub SaveRecipe()
        If txtNameRecipe.Text = "" Then
            MsgBox("Введите имя рецептуры и попробуйте сохранить снова")
            Exit Sub
        End If
        recipe.name = txtNameRecipe.Text
        recipe.SaveToDB()
    End Sub
    Sub fillDGVОсновныеКомпоненты()
        ЛипатоваРогова.ЭЦ(recipe)
        If dgvОсновныеКомпоненты.RowCount <> 0 Then
            Dim row As DataGridViewRow
            row = dgvОсновныеКомпоненты.Rows(0)

            row.Cells(0).Value = recipe.Sбелок
            row.Cells(1).Value = recipe.Sжир
            row.Cells(2).Value = recipe.Sуглеводы
        Else
            Dim row As New DataGridViewRow
            row.CreateCells(dgvОсновныеКомпоненты)

            row.Cells(0).Value = recipe.Sбелок
            row.Cells(1).Value = recipe.Sжир
            row.Cells(2).Value = recipe.Sуглеводы

            dgvОсновныеКомпоненты.Rows.Add(row)
        End If
    End Sub
    Sub fillЭЦ()
        If dgvЭнергетическаяЦенность.RowCount <> 0 Then
            Dim row As DataGridViewRow
            row = dgvЭнергетическаяЦенность.Rows(0)

            row.Cells(0).Value = recipe.ЭЦ_ккал
            row.Cells(1).Value = recipe.ЭЦ_кдж
        Else
            Dim row2 As New DataGridViewRow
        row2.CreateCells(dgvЭнергетическаяЦенность)
        row2.Cells(0).Value = recipe.ЭЦ_ккал
        row2.Cells(1).Value = recipe.ЭЦ_кдж
            dgvЭнергетическаяЦенность.Rows.Add(row2)
        End If
    End Sub
    Sub fillСкор()
        If dgvНак_Скор.RowCount <> 0 Then
            Dim row3 As DataGridViewRow
            row3 = dgvНак_Скор.Rows(0)

            For i% = 1 To 8
                row3.Cells(i).Value = Аминокислоты_М(i, recipe)
            Next

            Dim row4 As DataGridViewRow
            row4 = dgvНак_Скор.Rows(1)

            row4.Cells(1).Value = row3.Cells(1).Value / 4 * 100
            row4.Cells(2).Value = row3.Cells(2).Value / 7 * 100
            row4.Cells(3).Value = row3.Cells(3).Value / 5 * 100
            row4.Cells(4).Value = row3.Cells(4).Value / 3.5 * 100
            row4.Cells(5).Value = row3.Cells(5).Value / 6 * 100
            row4.Cells(6).Value = row3.Cells(6).Value / 1 * 100
            row4.Cells(7).Value = row3.Cells(7).Value / 5.5 * 100
            row4.Cells(8).Value = row3.Cells(8).Value / 4 * 100

            Dim Cmin As Double
            Cmin = Расчёт_Cmin(row4)

            Dim row5 As DataGridViewRow
            row5 = dgvНак_Скор.Rows(2)

            For i% = 1 To dgvНак_Скор.ColumnCount - 1
                row5.Cells(i).Value = Cmin / row4.Cells(i).Value
            Next
        Else
            Dim row3 As New DataGridViewRow
            row3.CreateCells(dgvНак_Скор)
            row3.Cells(0).Value = "М, %"
            row3.Cells(0).Style.BackColor = Color.Aqua
            For i% = 1 To 8
                row3.Cells(i).Value = Аминокислоты_М(i, recipe)
            Next
            dgvНак_Скор.Rows.Add(row3)

            Dim row4 As New DataGridViewRow
            row4.CreateCells(dgvНак_Скор)
            row4.Cells(0).Value = "С, %"
            row4.Cells(0).Style.BackColor = Color.Pink

            row4.Cells(1).Value = row3.Cells(1).Value / 4 * 100
            row4.Cells(2).Value = row3.Cells(2).Value / 7 * 100
            row4.Cells(3).Value = row3.Cells(3).Value / 5 * 100
            row4.Cells(4).Value = row3.Cells(4).Value / 3.5 * 100
            row4.Cells(5).Value = row3.Cells(5).Value / 6 * 100
            row4.Cells(6).Value = row3.Cells(6).Value / 1 * 100
            row4.Cells(7).Value = row3.Cells(7).Value / 5.5 * 100
            row4.Cells(8).Value = row3.Cells(8).Value / 4 * 100
            dgvНак_Скор.Rows.Add(row4)

            Dim Cmin As Double
            Cmin = Расчёт_Cmin(row4)
            'txtCmin.Text = Cmin

            Dim row5 As New DataGridViewRow
            row5.CreateCells(dgvНак_Скор)
            row5.Cells(0).Value = "a, %"
            row5.Cells(0).Style.BackColor = Color.Yellow
            For i% = 1 To dgvНак_Скор.ColumnCount - 1
                row5.Cells(i).Value = Cmin / row4.Cells(i).Value
            Next
            dgvНак_Скор.Rows.Add(row5)
        End If
    End Sub
    Sub fillБелковыйКомпонент()
        Dim row3 As DataGridViewRow = dgvНак_Скор.Rows(0)
        Dim row4 As DataGridViewRow = dgvНак_Скор.Rows(1)
        Dim row5 As DataGridViewRow = dgvНак_Скор.Rows(2)

        If dgvБелковыйКомпонент.RowCount <> 0 Then
            Dim row6 As DataGridViewRow
            row6 = dgvБелковыйКомпонент.Rows(0)

            row6.Cells(0).Value = Расчёт_КРАС(row4, recipe)
            row6.Cells(1).Value = 100 - row6.Cells(0).Value
            row6.Cells(2).Value = Расчёт_U(row3, row5, recipe)
            row6.Cells(3).Value = Расчёт_G(row3, row5, recipe)
        Else
            Dim row6 As New DataGridViewRow
            row6.CreateCells(dgvБелковыйКомпонент)
            row6.Cells(0).Value = Расчёт_КРАС(row4, recipe)
            row6.Cells(1).Value = 100 - row6.Cells(0).Value
            row6.Cells(2).Value = Расчёт_U(row3, row5, recipe)
            row6.Cells(3).Value = Расчёт_G(row3, row5, recipe)
            dgvБелковыйКомпонент.Rows.Add(row6)
        End If
    End Sub
    Private Sub НазваниеListBox_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles НазваниеListBox.MouseDoubleClick
        Dim codeRecipe As Long = НазваниеListBox.SelectedValue
        'MsgBox(НазваниеListBox.SelectedValue)
        recipe.AddIngredient(codeRecipe)
        dgvRecipe1.Rows.Clear()
        fillDGV(recipe)
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        IngredientsBindingSource.Filter = String.Format("{0} like '%{1}%'", НазваниеListBox.DisplayMember, TextBox1.Text)
    End Sub

    Private Sub GroupBox4_Enter(sender As Object, e As EventArgs) Handles GroupBox4.Enter

    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs) Handles Label5.Click

    End Sub

    Private Sub btnOptim_Click(sender As Object, e As EventArgs) Handles btnOptim.Click
        SaveRecipe()
    End Sub

    Private Sub txbNameRecipe_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Form1.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        For i% = 0 To dgvRecipe1.RowCount - 1
            Dim md#
            md = dgvRecipe1.Rows(i).Cells(1).Value
            recipe.ChangeMD(i, md)
        Next
        dgvRecipe1.Rows.Clear()
        fillDGV(recipe)
    End Sub
End Class