﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Оптимизация
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Оптимизация))
        Dim НазваниеLabel As System.Windows.Forms.Label
        Me.ViewReceptBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ViewReceptBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ViewReceptDataGridView = New System.Windows.Forms.DataGridView()
        Me.ViewReceptBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.ViewReceptTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.ViewReceptTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RecipesTableAdapter1 = New ProjectUMeat.ProductsDBDataSetTableAdapters.RecipesTableAdapter()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IngredientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IngredientsTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngredientsTableAdapter()
        Me.НазваниеListBox = New System.Windows.Forms.ListBox()
        НазваниеLabel = New System.Windows.Forms.Label()
        CType(Me.ViewReceptBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewReceptBindingNavigator.SuspendLayout()
        CType(Me.ViewReceptDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ViewReceptBindingNavigator
        '
        Me.ViewReceptBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.ViewReceptBindingNavigator.BindingSource = Me.ViewReceptBindingSource
        Me.ViewReceptBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ViewReceptBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ViewReceptBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.ViewReceptBindingNavigatorSaveItem})
        Me.ViewReceptBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ViewReceptBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ViewReceptBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ViewReceptBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ViewReceptBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ViewReceptBindingNavigator.Name = "ViewReceptBindingNavigator"
        Me.ViewReceptBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ViewReceptBindingNavigator.Size = New System.Drawing.Size(654, 25)
        Me.ViewReceptBindingNavigator.TabIndex = 0
        Me.ViewReceptBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Переместить в начало"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Переместить назад"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Положение"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Текущее положение"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(43, 22)
        Me.BindingNavigatorCountItem.Text = "для {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Общее число элементов"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Переместить вперед"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Переместить в конец"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Добавить"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Удалить"
        '
        'ViewReceptBindingNavigatorSaveItem
        '
        Me.ViewReceptBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ViewReceptBindingNavigatorSaveItem.Enabled = False
        Me.ViewReceptBindingNavigatorSaveItem.Image = CType(resources.GetObject("ViewReceptBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ViewReceptBindingNavigatorSaveItem.Name = "ViewReceptBindingNavigatorSaveItem"
        Me.ViewReceptBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.ViewReceptBindingNavigatorSaveItem.Text = "Сохранить данные"
        '
        'ViewReceptDataGridView
        '
        Me.ViewReceptDataGridView.AutoGenerateColumns = False
        Me.ViewReceptDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ViewReceptDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18})
        Me.ViewReceptDataGridView.DataSource = Me.ViewReceptBindingSource
        Me.ViewReceptDataGridView.Location = New System.Drawing.Point(12, 195)
        Me.ViewReceptDataGridView.Name = "ViewReceptDataGridView"
        Me.ViewReceptDataGridView.Size = New System.Drawing.Size(622, 239)
        Me.ViewReceptDataGridView.TabIndex = 1
        '
        'ViewReceptBindingSource
        '
        Me.ViewReceptBindingSource.DataMember = "ViewRecept"
        Me.ViewReceptBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ViewReceptTableAdapter
        '
        Me.ViewReceptTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.IngredientsTableAdapter = Nothing
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(224, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Label1"
        '
        'RecipesTableAdapter1
        '
        Me.RecipesTableAdapter1.ClearBeforeFill = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Название"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Название"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Массовая доля"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Массовая доля"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Белок"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Белок"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Жиры"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Жиры"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Углеводы"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Углеводы"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Зола"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Зола"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Клечатка"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Клечатка"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Изолейц"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Изолейц"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Лейц"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Лейц"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Валин"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Валин"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Мет+Цист"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Мет+Цист"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Фен+Тир"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Фен+Тир"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Триптофан"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Триптофан"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "Лизин"
        Me.DataGridViewTextBoxColumn16.HeaderText = "Лизин"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Треон"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Треон"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "Вода"
        Me.DataGridViewTextBoxColumn18.HeaderText = "Вода"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'IngredientsBindingSource
        '
        Me.IngredientsBindingSource.DataMember = "Ingredients"
        Me.IngredientsBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'IngredientsTableAdapter
        '
        Me.IngredientsTableAdapter.ClearBeforeFill = True
        '
        'НазваниеLabel
        '
        НазваниеLabel.AutoSize = True
        НазваниеLabel.Location = New System.Drawing.Point(25, 69)
        НазваниеLabel.Name = "НазваниеLabel"
        НазваниеLabel.Size = New System.Drawing.Size(60, 13)
        НазваниеLabel.TabIndex = 3
        НазваниеLabel.Text = "Название:"
        '
        'НазваниеListBox
        '
        Me.НазваниеListBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.IngredientsBindingSource, "Название", True))
        Me.НазваниеListBox.FormattingEnabled = True
        Me.НазваниеListBox.Location = New System.Drawing.Point(91, 69)
        Me.НазваниеListBox.Name = "НазваниеListBox"
        Me.НазваниеListBox.Size = New System.Drawing.Size(120, 95)
        Me.НазваниеListBox.TabIndex = 4
        '
        'Оптимизация
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 454)
        Me.Controls.Add(НазваниеLabel)
        Me.Controls.Add(Me.НазваниеListBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ViewReceptDataGridView)
        Me.Controls.Add(Me.ViewReceptBindingNavigator)
        Me.Name = "Оптимизация"
        Me.Text = "Оптимизация"
        CType(Me.ViewReceptBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewReceptBindingNavigator.ResumeLayout(False)
        Me.ViewReceptBindingNavigator.PerformLayout()
        CType(Me.ViewReceptDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents ViewReceptBindingSource As BindingSource
    Friend WithEvents ViewReceptTableAdapter As ProductsDBDataSetTableAdapters.ViewReceptTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ViewReceptBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents ViewReceptBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents ViewReceptDataGridView As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents RecipesTableAdapter1 As ProductsDBDataSetTableAdapters.RecipesTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents IngredientsBindingSource As BindingSource
    Friend WithEvents IngredientsTableAdapter As ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Friend WithEvents НазваниеListBox As ListBox
End Class
