﻿Public Class SelectorIngredients
    Private Sub SelectorIngredients_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        IngredientsTableAdapter.Fill(ProductsDBDataSet.Ingredients)
    End Sub
    Public selectedIngredient As Long
    Private Sub IngredientsDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles IngredientsDataGridView.CellDoubleClick
        Dim nameSelected As String
        nameSelected = IngredientsDataGridView.Rows(e.RowIndex).Cells(0).Value

        Dim codeSelected As Long
        codeSelected = GetCodeIngredientByName(nameSelected)
        selectedIngredient = codeSelected

        Me.Close()
    End Sub
End Class