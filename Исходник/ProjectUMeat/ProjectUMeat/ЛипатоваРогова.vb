﻿Module ЛипатоваРогова
    'Расчёт Энергетической ценности
    Public Sub ЭЦ(ByRef recipe As clRecipe)
        recipe = S_Components(recipe)
        recipe.ЭЦ_ккал = ЭЦ_ккал(recipe)
        recipe.ЭЦ_кдж = ЭЦ_кдж(recipe)
    End Sub
    Public Function S_Components(recipe As clRecipe) As clRecipe
        recipe.Sбелок = S_component_белок(recipe)
        recipe.Sжир = S_component_жир(recipe)
        recipe.Sуглеводы = S_component_углеводы(recipe)

        Return recipe
    End Function
    Public Function ЭЦ_ккал(recipe As clRecipe) As Integer
        Try
            Return recipe.Sбелок * 4 + recipe.Sжир * 9 + recipe.Sуглеводы * 4
        Catch ex As Exception
            recipe.Sбелок = 0
            recipe.Sжир = 0
            recipe.Sуглеводы = 0
            Return 0
        End Try
    End Function
    Public Function ЭЦ_кдж(recipe As clRecipe) As Integer
        Return recipe.Sбелок * 17 + recipe.Sжир * 37 + recipe.Sуглеводы * 4
    End Function
    'Повторяющиеся куски кода в функциях S_Components, нужно оптимизировать, к тому же циклов много получается
    Private Function S_component_белок(recipe As clRecipe) As Double
        Try
            Dim доляКомпонента As Double = 0
            Dim суммаДоли As Double = 0
            For i% = 0 To recipe.CompositionRecipe.Length - 1
                доляКомпонента = доляКомпонента + recipe.CompositionRecipe(i).Md * recipe.CompositionRecipe(i).Ingredient.components.белок
                суммаДоли = суммаДоли + recipe.CompositionRecipe(i).Md
            Next
            Return доляКомпонента / суммаДоли
        Catch ex As Exception
        End Try
    End Function
    Private Function S_component_жир(recipe As clRecipe) As Double
        Dim доляКомпонента As Double = 0
        Dim суммаДоли As Double = 0
        For i% = 0 To recipe.CompositionRecipe.Length - 1
            доляКомпонента = доляКомпонента + recipe.CompositionRecipe(i).Md * recipe.CompositionRecipe(i).Ingredient.components.жиры
            суммаДоли = суммаДоли + recipe.CompositionRecipe(i).Md
        Next
        Return доляКомпонента / суммаДоли
    End Function
    Private Function S_component_углеводы(recipe As clRecipe) As Double
        Dim доляКомпонента As Double = 0
        Dim суммаДоли As Double = 0
        For i% = 0 To recipe.CompositionRecipe.Length - 1
            доляКомпонента = доляКомпонента + recipe.CompositionRecipe(i).Md * recipe.CompositionRecipe(i).Ingredient.components.углеводы
            суммаДоли = суммаДоли + recipe.CompositionRecipe(i).Md
        Next
        Return доляКомпонента / суммаДоли
    End Function
    'Конец Расчёт Энергетической ценности

    'Расчёт цены рецепта
    Public Function Цена(recipe As clRecipe) As Double
        Dim priceMd As Double = 0
        Dim md As Double = 0
        For i% = 0 To recipe.CompositionRecipe.Length - 1
            md = md + recipe.CompositionRecipe(i).Md
            priceMd = priceMd + recipe.CompositionRecipe(i).Ingredient.price / md
        Next
        priceMd = priceMd * 100 / md
        Return priceMd
    End Function
    'Конец Расчёт цены рецепта

    'Расчёт Аминокислот
    Public Sub Aminokisloty(ByRef recipe As clRecipe)

    End Sub
    Private мд_белок As Double = 0
    Public Function Аминокислоты_М(indАминокислота As Integer, recipe As clRecipe) As Double
        If мд_белок = 0 Then
            мд_белок = ОбщееСодержаниеБелка(indАминокислота, recipe)
            'MsgBox(мд_белок)
        End If

        Dim мд_белок_аминокислота As Double = 0
        For i% = 0 To recipe.CompositionRecipe.Length - 1
            Dim g As Double = recipe.CompositionRecipe(i).Md * recipe.CompositionRecipe(i).Ingredient.components.белок
            Dim am As Double
            Select Case indАминокислота
                Case 1 : am = recipe.CompositionRecipe(i).Ingredient.components.изолейц
                Case 2 : am = recipe.CompositionRecipe(i).Ingredient.components.лейц
                Case 3 : am = recipe.CompositionRecipe(i).Ingredient.components.валин
                Case 4 : am = recipe.CompositionRecipe(i).Ingredient.components.мет_цист
                Case 5 : am = recipe.CompositionRecipe(i).Ingredient.components.фен_тир
                Case 6 : am = recipe.CompositionRecipe(i).Ingredient.components.триптофан
                Case 7 : am = recipe.CompositionRecipe(i).Ingredient.components.лизин
                Case 8 : am = recipe.CompositionRecipe(i).Ingredient.components.треон
            End Select
            мд_белок_аминокислота = мд_белок_аминокислота + g * am
            'MsgBox(мд_белок_аминокислота)
            'мд_белок_аминокислота = мд_белок_аминокислота + am
        Next

        Return мд_белок_аминокислота / мд_белок
    End Function
    Private Function ОбщееСодержаниеБелка(indАминокислота As Integer, recipe As clRecipe) As Double
        Dim доляКомпонента As Double = 0
        Dim суммаДоли As Double = 0
        Dim am As Double
        For i% = 0 To recipe.CompositionRecipe.Length - 1
            Select Case indАминокислота
                Case 1 : am = recipe.CompositionRecipe(i).Ingredient.components.изолейц
                Case 2 : am = recipe.CompositionRecipe(i).Ingredient.components.лейц
                Case 3 : am = recipe.CompositionRecipe(i).Ingredient.components.валин
                Case 4 : am = recipe.CompositionRecipe(i).Ingredient.components.мет_цист
                Case 5 : am = recipe.CompositionRecipe(i).Ingredient.components.фен_тир
                Case 6 : am = recipe.CompositionRecipe(i).Ingredient.components.триптофан
                Case 7 : am = recipe.CompositionRecipe(i).Ingredient.components.лизин
                Case 8 : am = recipe.CompositionRecipe(i).Ingredient.components.треон
            End Select
            If am <> 0 Then
                доляКомпонента = доляКомпонента + recipe.CompositionRecipe(i).Md * recipe.CompositionRecipe(i).Ingredient.components.белок
            End If
        Next
        Return доляКомпонента
    End Function

    Dim Cmin As Double
    Public Function Расчёт_Cmin(row As DataGridViewRow) As Double
        Dim min As Double
        min = row.Cells(1).Value
        For i% = 1 To row.Cells.Count - 1
            min = Math.Min(min, row.Cells(i).Value)
        Next
        Cmin = min
        Return min
    End Function

    Public Function Расчёт_КРАС(row As DataGridViewRow, recipe As clRecipe) As Double
        Dim КРАС As Double = 0
        For i% = 1 To row.Cells.Count - 1
            КРАС = КРАС + row.Cells(i).Value - Cmin
        Next
        Return КРАС / 8
    End Function
    Private Function SumАминокислот(indАминокислота%, recipe As clRecipe) As Double
        Dim sum As Double = 0
        Dim am As Double
        For i% = 0 To recipe.CompositionRecipe.Length - 1
            Select Case indАминокислота
                Case 1 : am = recipe.CompositionRecipe(i).Ingredient.components.изолейц
                Case 2 : am = recipe.CompositionRecipe(i).Ingredient.components.лейц
                Case 3 : am = recipe.CompositionRecipe(i).Ingredient.components.валин
                Case 4 : am = recipe.CompositionRecipe(i).Ingredient.components.мет_цист
                Case 5 : am = recipe.CompositionRecipe(i).Ingredient.components.фен_тир
                Case 6 : am = recipe.CompositionRecipe(i).Ingredient.components.триптофан
                Case 7 : am = recipe.CompositionRecipe(i).Ingredient.components.лизин
                Case 8 : am = recipe.CompositionRecipe(i).Ingredient.components.треон
            End Select
            'MsgBox(recipe.CompositionRecipe(i).Ingredient.components.лизин)
            sum = sum + am
        Next
        Return sum
    End Function
    Public Function Расчёт_U(row1 As DataGridViewRow, row2 As DataGridViewRow, recipe As clRecipe) As Double
        Dim sumU As Double = 0
        Dim sumA As Double = 0
        For i% = 1 To row1.Cells.Count - 1
            sumU = sumU + row1.Cells(i).Value * row2.Cells(i).Value
            sumA = sumA + row1.Cells(i).Value
        Next
        'MsgBox(sumA)
        Return sumU / sumA
    End Function
    Public Function Расчёт_G(row1 As DataGridViewRow, row2 As DataGridViewRow, recipe As clRecipe) As Double
        Dim sumG As Double = 0
        For i% = 1 To row1.Cells.Count - 1
            sumG = sumG + (1 - row2.Cells(i).Value) * row1.Cells(i).Value
        Next
        Return sumG / (Cmin / 100)
    End Function
    'Конец Расчёт Аминокислот
End Module
