﻿Imports ProjectUMeat.frmSimplex
Imports ZedGraph
Public Class OutputOptim
    Private recipe1 As clRecipe
    Private recipe2 As clRecipe

    Dim x() As Double
    Dim confines() As structConfines

    Public Sub output(x#(), rec As clRecipe, confines() As structConfines)
        Try
            Me.x = x
            Me.Show()
            recipe1 = rec
            Me.confines = confines

            dgvConfines.Columns.Add("Название_компонента", "Название компонента")
            dgvConfines.Columns.Add("Значение_в_рецепта", "Значение в рецепте")
            dgvConfines.Columns.Add("Равенство", "Равенство")
            dgvConfines.Columns.Add("Значение", "Значение")

            Dim i%
            For Each p In confines
                Dim row As New DataGridViewRow
                row.CreateCells(dgvConfines)
                row.Cells(0).Value = p.name

                row.Cells(2).Value = p.equality
                row.Cells(3).Value = p.value
                dgvConfines.Rows.Add(row)
                i += 1
            Next



            viewRecipe1()
            recipe1.all()
            DrawGraph1()
            newRecipe(x)
            viewRecipe2()



        Catch ex As Exception
            MsgBox(1)
        End Try

        recipe2.all()
        dgvConfines.Rows(0).Cells(1).Value = recipe2.белок
        dgvConfines.Rows(1).Cells(1).Value = recipe2.жиры
        dgvConfines.Rows(2).Cells(1).Value = recipe2.углеводы
        dgvConfines.Rows(3).Cells(1).Value = recipe2.сухие_вещества

        DrawGraph2()
    End Sub
    Private Sub outputDGV()

    End Sub
    Private Sub newRecipe(x#())
        recipe2 = recipe1
        For i% = 0 To recipe2.CompositionRecipe.Length - 1
            recipe2.ChangeMD(i, x(i))
        Next
    End Sub
    Private Sub viewRecipe1()
        Dim dgv As DataGridView = dgvRecipe1
        dgv.Columns.Add("Рецептурный_ингредиент", "Рецептурный ингредиент")
        dgv.Columns.Add("Массовая_доля", "Массовая доля")
        For Each x As clRecipe.structCompositionRecipe In recipe1.CompositionRecipe
            Dim row As New DataGridViewRow
            row.CreateCells(dgv)
            row.Cells(0).Value = x.Ingredient.name
            row.Cells(1).Value = x.Md
            dgv.Rows.Add(row)
        Next
    End Sub
    Private Sub viewRecipe2()
        Dim dgv As DataGridView = dgvRecipe2
        dgv.Columns.Add("Рецептурный_ингредиент", "Рецептурный ингредиент")
        dgv.Columns.Add("Массовая_доля", "Массовая доля")
        For Each x As clRecipe.structCompositionRecipe In recipe1.CompositionRecipe
            Dim row As New DataGridViewRow
            row.CreateCells(dgv)
            row.Cells(0).Value = x.Ingredient.name
            row.Cells(1).Value = x.Md
            dgv.Rows.Add(row)
        Next
    End Sub
    Private Sub DrawGraph1()
        'Получим панель для рисования
        Dim pane As GraphPane = ZedGraphControl1.GraphPane

        'Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
        pane.CurveList.Clear()

        Dim itemscount% = 5

        Dim Rnd As New Random

        ' Высоты столбиков

        Dim YValues1#(itemscount)
        Dim YValues2#(itemscount)
        Dim YValues3#(itemscount)

        Dim xvalues#(itemscount)
        Dim yvalues#(itemscount)

        ' Заполним данные

        For i% = 0 To itemscount - 1
            xvalues(i) = i + 1
            yvalues(i) = Rnd.NextDouble()
        Next
        ' Создадим три гистограммы
        ' Так как для всех гистограмм мы передаем одинаковые массивы координат по X,
        ' то столбики будут группироваться в кластеры в этих точках.
        'Dim bar1 As BarItem = pane.AddBar("Values1", XValues, YValues1, Color.Blue)
        'Dim bar2 As BarItem = pane.AddBar("Values2", XValues, YValues2, Color.Red)
        'Dim bar3 As BarItem = pane.AddBar("Values3", XValues, YValues3, Color.Yellow)


        ' !!! Расстояния между столбиками в кластере (группами столбиков)
        pane.BarSettings.MinBarGap = 1

        ' Создадим кривую-гистограмму
        Dim curve1 As BarItem = pane.AddBar("Белки", {1}, {recipe1.белок}, Color.Blue)
        Dim curve2 As BarItem = pane.AddBar("Жиры", {2}, {recipe1.жиры}, Color.Blue)
        Dim curve3 As BarItem = pane.AddBar("Углеводы", {3}, {recipe1.углеводы}, Color.Orange)

        pane.XAxis.Type = AxisType.Text

        Dim names(5) As String
        names(0) = "Белки"
        names(1) = "Жиры"
        names(2) = "Углеводы"
        pane.XAxis.Scale.TextLabels = names

        ' !!!
        ' Установим цвет для столбцов гистограммы
        curve1.Bar.Fill.Color = Color.YellowGreen

        ' Отключим градиентную заливку
        curve1.Bar.Fill.Type = FillType.Solid

        ' Сделаем границы столбцов невидимыми
        curve1.Bar.Border.IsVisible = False
        ' !!! Увеличим расстояние между кластерами в 2.5 раза
        'pane.BarSettings.MinBarGap = 2.5

        'Вызываем метод AxisChange (), чтобы обновить данные об осях. 
        'В противном случае на рисунке будет показана только часть графика, 
        'которая умещается в интервалы по осям, установленные по умолчанию
        ZedGraphControl1.AxisChange()

        'Обновляем график
        ZedGraphControl1.Invalidate()
    End Sub
    Private Sub DrawGraph2()
        'Получим панель для рисования
        Dim pane As GraphPane = ZedGraphControl2.GraphPane

        'Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
        pane.CurveList.Clear()

        Dim itemscount% = 5

        Dim Rnd As New Random

        ' Высоты столбиков

        Dim YValues1#(itemscount)
        Dim YValues2#(itemscount)
        Dim YValues3#(itemscount)

        Dim xvalues#(itemscount)
        Dim yvalues#(itemscount)

        ' Заполним данные

        For i% = 0 To itemscount - 1
            xvalues(i) = i + 1
            yvalues(i) = Rnd.NextDouble()
        Next
        ' Создадим три гистограммы
        ' Так как для всех гистограмм мы передаем одинаковые массивы координат по X,
        ' то столбики будут группироваться в кластеры в этих точках.
        'Dim bar1 As BarItem = pane.AddBar("Values1", XValues, YValues1, Color.Blue)
        'Dim bar2 As BarItem = pane.AddBar("Values2", XValues, YValues2, Color.Red)
        'Dim bar3 As BarItem = pane.AddBar("Values3", XValues, YValues3, Color.Yellow)


        ' !!! Расстояния между столбиками в кластере (группами столбиков)
        pane.BarSettings.MinBarGap = 1

        ' Создадим кривую-гистограмму
        Dim curve1 As BarItem = pane.AddBar("Белки", {0}, {recipe2.белок}, Color.Blue)
        Dim curve2 As BarItem = pane.AddBar("Жиры", {1}, {recipe2.жиры}, Color.Blue)
        Dim curve3 As BarItem = pane.AddBar("Углеводы", {2}, {recipe2.углеводы}, Color.Orange)

        pane.XAxis.Type = AxisType.Text

        Dim names(5) As String
        names(0) = "Белки"
        names(1) = "Жиры"
        names(2) = "Углеводы"
        pane.XAxis.Scale.TextLabels = names

        ' !!!
        ' Установим цвет для столбцов гистограммы
        curve1.Bar.Fill.Color = Color.YellowGreen

        ' Отключим градиентную заливку
        curve1.Bar.Fill.Type = FillType.Solid

        ' Сделаем границы столбцов невидимыми
        curve1.Bar.Border.IsVisible = False
        ' !!! Увеличим расстояние между кластерами в 2.5 раза
        'pane.BarSettings.MinClusterGap = 2.5

        'Вызываем метод AxisChange (), чтобы обновить данные об осях. 
        'В противном случае на рисунке будет показана только часть графика, 
        'которая умещается в интервалы по осям, установленные по умолчанию
        ZedGraphControl2.AxisChange()

        'Обновляем график
        ZedGraphControl2.Invalidate()
    End Sub
End Class