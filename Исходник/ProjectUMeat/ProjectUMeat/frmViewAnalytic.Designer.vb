﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmViewAnalytic
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Me.dgvОсновныеКомпоненты = New System.Windows.Forms.DataGridView()
        Me.Sбелка = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sжира = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sуглеводов = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvНак_Скор = New System.Windows.Forms.DataGridView()
        Me.тип = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.изолейц = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.лейц = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.валин = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.мет_цист = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.фен_тир = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.триптофан = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.лизин = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.треон = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvЭнергетическаяЦенность = New System.Windows.Forms.DataGridView()
        Me.ккал = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.кДж = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvБелковыйКомпонент = New System.Windows.Forms.DataGridView()
        Me.КРАС = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.БЦ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.U = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.G = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Массовая_доля = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Белок = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Жиры = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Углеводы = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Клечатка = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtCmin = New System.Windows.Forms.TextBox()
        Me.ViewReceptBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.ViewReceptTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.ViewReceptTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.chrБелковыйПоказатель = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RecipesTableAdapter1 = New ProjectUMeat.ProductsDBDataSetTableAdapters.RecipesTableAdapter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        CType(Me.dgvОсновныеКомпоненты, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvНак_Скор, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvЭнергетическаяЦенность, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvБелковыйКомпонент, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chrБелковыйПоказатель, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvОсновныеКомпоненты
        '
        Me.dgvОсновныеКомпоненты.AllowUserToAddRows = False
        Me.dgvОсновныеКомпоненты.AllowUserToDeleteRows = False
        Me.dgvОсновныеКомпоненты.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvОсновныеКомпоненты.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvОсновныеКомпоненты.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvОсновныеКомпоненты.ColumnHeadersHeight = 30
        Me.dgvОсновныеКомпоненты.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvОсновныеКомпоненты.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sбелка, Me.Sжира, Me.Sуглеводов})
        Me.dgvОсновныеКомпоненты.Location = New System.Drawing.Point(6, 89)
        Me.dgvОсновныеКомпоненты.MultiSelect = False
        Me.dgvОсновныеКомпоненты.Name = "dgvОсновныеКомпоненты"
        Me.dgvОсновныеКомпоненты.ReadOnly = True
        Me.dgvОсновныеКомпоненты.RowHeadersVisible = False
        Me.dgvОсновныеКомпоненты.RowHeadersWidth = 50
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.dgvОсновныеКомпоненты.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvОсновныеКомпоненты.RowTemplate.ReadOnly = True
        Me.dgvОсновныеКомпоненты.Size = New System.Drawing.Size(322, 64)
        Me.dgvОсновныеКомпоненты.TabIndex = 4
        '
        'Sбелка
        '
        Me.Sбелка.DataPropertyName = "Sбелка"
        Me.Sбелка.HeaderText = "Sбелка, %"
        Me.Sбелка.Name = "Sбелка"
        Me.Sбелка.ReadOnly = True
        '
        'Sжира
        '
        Me.Sжира.HeaderText = "Sжира, %"
        Me.Sжира.Name = "Sжира"
        Me.Sжира.ReadOnly = True
        '
        'Sуглеводов
        '
        Me.Sуглеводов.HeaderText = "Sуглеводов, %"
        Me.Sуглеводов.Name = "Sуглеводов"
        Me.Sуглеводов.ReadOnly = True
        '
        'dgvНак_Скор
        '
        Me.dgvНак_Скор.AllowUserToAddRows = False
        Me.dgvНак_Скор.AllowUserToDeleteRows = False
        Me.dgvНак_Скор.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvНак_Скор.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvНак_Скор.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvНак_Скор.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvНак_Скор.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.тип, Me.изолейц, Me.лейц, Me.валин, Me.мет_цист, Me.фен_тир, Me.триптофан, Me.лизин, Me.треон})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.Format = "0.###"
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvНак_Скор.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvНак_Скор.Location = New System.Drawing.Point(6, 179)
        Me.dgvНак_Скор.MultiSelect = False
        Me.dgvНак_Скор.Name = "dgvНак_Скор"
        Me.dgvНак_Скор.ReadOnly = True
        Me.dgvНак_Скор.RowHeadersVisible = False
        Me.dgvНак_Скор.Size = New System.Drawing.Size(683, 108)
        Me.dgvНак_Скор.TabIndex = 5
        '
        'тип
        '
        Me.тип.HeaderText = ""
        Me.тип.Name = "тип"
        Me.тип.ReadOnly = True
        '
        'изолейц
        '
        Me.изолейц.HeaderText = "изолейц"
        Me.изолейц.Name = "изолейц"
        Me.изолейц.ReadOnly = True
        '
        'лейц
        '
        Me.лейц.HeaderText = "лейц"
        Me.лейц.Name = "лейц"
        Me.лейц.ReadOnly = True
        '
        'валин
        '
        Me.валин.HeaderText = "валин"
        Me.валин.Name = "валин"
        Me.валин.ReadOnly = True
        '
        'мет_цист
        '
        Me.мет_цист.HeaderText = "мет=цист"
        Me.мет_цист.Name = "мет_цист"
        Me.мет_цист.ReadOnly = True
        '
        'фен_тир
        '
        Me.фен_тир.HeaderText = "фен=тир"
        Me.фен_тир.Name = "фен_тир"
        Me.фен_тир.ReadOnly = True
        '
        'триптофан
        '
        Me.триптофан.HeaderText = "триптофан"
        Me.триптофан.Name = "триптофан"
        Me.триптофан.ReadOnly = True
        '
        'лизин
        '
        Me.лизин.HeaderText = "лизин"
        Me.лизин.Name = "лизин"
        Me.лизин.ReadOnly = True
        '
        'треон
        '
        Me.треон.HeaderText = "треон"
        Me.треон.Name = "треон"
        Me.треон.ReadOnly = True
        '
        'dgvЭнергетическаяЦенность
        '
        Me.dgvЭнергетическаяЦенность.AllowUserToAddRows = False
        Me.dgvЭнергетическаяЦенность.AllowUserToDeleteRows = False
        Me.dgvЭнергетическаяЦенность.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvЭнергетическаяЦенность.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvЭнергетическаяЦенность.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvЭнергетическаяЦенность.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvЭнергетическаяЦенность.ColumnHeadersHeight = 30
        Me.dgvЭнергетическаяЦенность.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvЭнергетическаяЦенность.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ккал, Me.кДж})
        Me.dgvЭнергетическаяЦенность.Location = New System.Drawing.Point(780, 297)
        Me.dgvЭнергетическаяЦенность.MultiSelect = False
        Me.dgvЭнергетическаяЦенность.Name = "dgvЭнергетическаяЦенность"
        Me.dgvЭнергетическаяЦенность.ReadOnly = True
        Me.dgvЭнергетическаяЦенность.RowHeadersVisible = False
        Me.dgvЭнергетическаяЦенность.RowHeadersWidth = 50
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.dgvЭнергетическаяЦенность.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvЭнергетическаяЦенность.Size = New System.Drawing.Size(322, 59)
        Me.dgvЭнергетическаяЦенность.TabIndex = 6
        '
        'ккал
        '
        Me.ккал.HeaderText = "ккал"
        Me.ккал.Name = "ккал"
        Me.ккал.ReadOnly = True
        '
        'кДж
        '
        Me.кДж.HeaderText = "кДж"
        Me.кДж.Name = "кДж"
        Me.кДж.ReadOnly = True
        '
        'dgvБелковыйКомпонент
        '
        Me.dgvБелковыйКомпонент.AllowUserToAddRows = False
        Me.dgvБелковыйКомпонент.AllowUserToDeleteRows = False
        Me.dgvБелковыйКомпонент.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvБелковыйКомпонент.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvБелковыйКомпонент.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvБелковыйКомпонент.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvБелковыйКомпонент.ColumnHeadersHeight = 35
        Me.dgvБелковыйКомпонент.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvБелковыйКомпонент.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.КРАС, Me.БЦ, Me.U, Me.G})
        Me.dgvБелковыйКомпонент.Location = New System.Drawing.Point(780, 397)
        Me.dgvБелковыйКомпонент.MultiSelect = False
        Me.dgvБелковыйКомпонент.Name = "dgvБелковыйКомпонент"
        Me.dgvБелковыйКомпонент.ReadOnly = True
        Me.dgvБелковыйКомпонент.RowHeadersVisible = False
        Me.dgvБелковыйКомпонент.RowHeadersWidth = 50
        Me.dgvБелковыйКомпонент.Size = New System.Drawing.Size(322, 63)
        Me.dgvБелковыйКомпонент.TabIndex = 7
        '
        'КРАС
        '
        Me.КРАС.HeaderText = "КРАС"
        Me.КРАС.Name = "КРАС"
        Me.КРАС.ReadOnly = True
        '
        'БЦ
        '
        Me.БЦ.HeaderText = "БЦ"
        Me.БЦ.Name = "БЦ"
        Me.БЦ.ReadOnly = True
        '
        'U
        '
        Me.U.HeaderText = "U"
        Me.U.Name = "U"
        Me.U.ReadOnly = True
        '
        'G
        '
        Me.G.HeaderText = "G"
        Me.G.Name = "G"
        Me.G.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Название"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Название"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'Массовая_доля
        '
        Me.Массовая_доля.DataPropertyName = "Массовая доля"
        Me.Массовая_доля.HeaderText = "Массовая доля"
        Me.Массовая_доля.Name = "Массовая_доля"
        Me.Массовая_доля.ReadOnly = True
        '
        'Белок
        '
        Me.Белок.DataPropertyName = "Белок"
        Me.Белок.HeaderText = "Белок"
        Me.Белок.Name = "Белок"
        Me.Белок.ReadOnly = True
        '
        'Жиры
        '
        Me.Жиры.DataPropertyName = "Жиры"
        Me.Жиры.HeaderText = "Жиры"
        Me.Жиры.Name = "Жиры"
        Me.Жиры.ReadOnly = True
        '
        'Углеводы
        '
        Me.Углеводы.DataPropertyName = "Углеводы"
        Me.Углеводы.HeaderText = "Углеводы"
        Me.Углеводы.Name = "Углеводы"
        Me.Углеводы.ReadOnly = True
        '
        'Клечатка
        '
        Me.Клечатка.DataPropertyName = "Клечатка"
        Me.Клечатка.HeaderText = "Клечатка"
        Me.Клечатка.Name = "Клечатка"
        Me.Клечатка.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Зола"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Зола"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Изолейц"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Изолейц"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Лейц"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Лейц"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Валин"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Валин"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'txtCmin
        '
        Me.txtCmin.Location = New System.Drawing.Point(42, 293)
        Me.txtCmin.Name = "txtCmin"
        Me.txtCmin.Size = New System.Drawing.Size(100, 20)
        Me.txtCmin.TabIndex = 9
        '
        'ViewReceptBindingSource
        '
        Me.ViewReceptBindingSource.DataMember = "ViewRecept"
        Me.ViewReceptBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ViewReceptTableAdapter
        '
        Me.ViewReceptTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.IngredientsTableAdapter = Nothing
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'chrБелковыйПоказатель
        '
        Me.chrБелковыйПоказатель.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chrБелковыйПоказатель.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Sunken
        ChartArea1.Name = "ChartArea1"
        Me.chrБелковыйПоказатель.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.chrБелковыйПоказатель.Legends.Add(Legend1)
        Me.chrБелковыйПоказатель.Location = New System.Drawing.Point(802, 35)
        Me.chrБелковыйПоказатель.Name = "chrБелковыйПоказатель"
        Me.chrБелковыйПоказатель.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry
        Me.chrБелковыйПоказатель.Size = New System.Drawing.Size(300, 220)
        Me.chrБелковыйПоказатель.TabIndex = 11
        Me.chrБелковыйПоказатель.Text = "Chart1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 20)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Label1"
        '
        'RecipesTableAdapter1
        '
        Me.RecipesTableAdapter1.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 302)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Cmin"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Green
        Me.Label3.Location = New System.Drawing.Point(844, 270)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(203, 19)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Энергетическая ценность"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Green
        Me.Label4.Location = New System.Drawing.Point(844, 375)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(197, 19)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Биологическая ценность"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 70)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(289, 16)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Общее содержание основных компонентов"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 156)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(204, 16)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Характеристики аминокислот"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.dgvНак_Скор)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtCmin)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dgvОсновныеКомпоненты)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 270)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(708, 358)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Биологические свойства"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 35)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(712, 220)
        Me.DataGridView1.TabIndex = 20
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(913, 563)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Отчет"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'ReportViewer1
        '

        '
        'frmViewAnalytic
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1120, 640)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.chrБелковыйПоказатель)
        Me.Controls.Add(Me.dgvБелковыйКомпонент)
        Me.Controls.Add(Me.dgvЭнергетическаяЦенность)
        Me.MinimizeBox = False
        Me.Name = "frmViewAnalytic"
        Me.Text = "Расчёты Липатова - Рогова"
        CType(Me.dgvОсновныеКомпоненты, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvНак_Скор, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvЭнергетическаяЦенность, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvБелковыйКомпонент, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chrБелковыйПоказатель, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvОсновныеКомпоненты As DataGridView
    Friend WithEvents dgvНак_Скор As DataGridView
    Friend WithEvents dgvЭнергетическаяЦенность As DataGridView
    Friend WithEvents ккал As DataGridViewTextBoxColumn
    Friend WithEvents кДж As DataGridViewTextBoxColumn
    Friend WithEvents dgvБелковыйКомпонент As DataGridView
    Friend WithEvents КРАС As DataGridViewTextBoxColumn
    Friend WithEvents БЦ As DataGridViewTextBoxColumn
    Friend WithEvents U As DataGridViewTextBoxColumn
    Friend WithEvents G As DataGridViewTextBoxColumn
    Friend WithEvents Sбелка As DataGridViewTextBoxColumn
    Friend WithEvents Sжира As DataGridViewTextBoxColumn
    Friend WithEvents Sуглеводов As DataGridViewTextBoxColumn
    Friend WithEvents тип As DataGridViewTextBoxColumn
    Friend WithEvents изолейц As DataGridViewTextBoxColumn
    Friend WithEvents лейц As DataGridViewTextBoxColumn
    Friend WithEvents валин As DataGridViewTextBoxColumn
    Friend WithEvents мет_цист As DataGridViewTextBoxColumn
    Friend WithEvents фен_тир As DataGridViewTextBoxColumn
    Friend WithEvents триптофан As DataGridViewTextBoxColumn
    Friend WithEvents лизин As DataGridViewTextBoxColumn
    Friend WithEvents треон As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents Массовая_доля As DataGridViewTextBoxColumn
    Friend WithEvents Белок As DataGridViewTextBoxColumn
    Friend WithEvents Жиры As DataGridViewTextBoxColumn
    Friend WithEvents Углеводы As DataGridViewTextBoxColumn
    Friend WithEvents Клечатка As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents txtCmin As TextBox
    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents ViewReceptBindingSource As BindingSource
    Friend WithEvents ViewReceptTableAdapter As ProductsDBDataSetTableAdapters.ViewReceptTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents chrБелковыйПоказатель As DataVisualization.Charting.Chart
    Friend WithEvents Label1 As Label
    Friend WithEvents RecipesTableAdapter1 As ProductsDBDataSetTableAdapters.RecipesTableAdapter
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Private WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
End Class
