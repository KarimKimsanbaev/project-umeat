﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class inputSettingsOptim
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim GroupBox3 As System.Windows.Forms.GroupBox
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inputSettingsOptim))
        Me.dgvSettingMD = New System.Windows.Forms.DataGridView()
        Me.cmbF = New System.Windows.Forms.ComboBox()
        Me.cmbБелок = New System.Windows.Forms.ComboBox()
        Me.cmb2 = New System.Windows.Forms.ComboBox()
        Me.cmb3 = New System.Windows.Forms.ComboBox()
        Me.dgvRecipe = New System.Windows.Forms.DataGridView()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbССВ = New System.Windows.Forms.ComboBox()
        Me.txtССВ = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtUgl = New System.Windows.Forms.TextBox()
        Me.txtЖир = New System.Windows.Forms.TextBox()
        Me.txtБелок = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnOptim = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        GroupBox3 = New System.Windows.Forms.GroupBox()
        GroupBox3.SuspendLayout()
        CType(Me.dgvSettingMD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRecipe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        GroupBox3.Controls.Add(Me.dgvSettingMD)
        GroupBox3.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        GroupBox3.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        GroupBox3.Location = New System.Drawing.Point(923, 12)
        GroupBox3.Name = "GroupBox3"
        GroupBox3.Size = New System.Drawing.Size(453, 182)
        GroupBox3.TabIndex = 21
        GroupBox3.TabStop = False
        GroupBox3.Text = "Варьирование Массовой доли ингредиентов (%)"
        '
        'dgvSettingMD
        '
        Me.dgvSettingMD.AllowUserToAddRows = False
        Me.dgvSettingMD.AllowUserToDeleteRows = False
        Me.dgvSettingMD.AllowUserToResizeColumns = False
        Me.dgvSettingMD.AllowUserToResizeRows = False
        Me.dgvSettingMD.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvSettingMD.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSettingMD.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSettingMD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSettingMD.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvSettingMD.Location = New System.Drawing.Point(6, 25)
        Me.dgvSettingMD.Name = "dgvSettingMD"
        Me.dgvSettingMD.RowHeadersVisible = False
        Me.dgvSettingMD.Size = New System.Drawing.Size(441, 152)
        Me.dgvSettingMD.TabIndex = 22
        '
        'cmbF
        '
        Me.cmbF.BackColor = System.Drawing.Color.GhostWhite
        Me.cmbF.Font = New System.Drawing.Font("Microsoft YaHei UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.cmbF.ForeColor = System.Drawing.Color.Black
        Me.cmbF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmbF.Items.AddRange(New Object() {"Цена", "Энергетичесая ценность"})
        Me.cmbF.Location = New System.Drawing.Point(32, 65)
        Me.cmbF.Name = "cmbF"
        Me.cmbF.Size = New System.Drawing.Size(234, 25)
        Me.cmbF.TabIndex = 4
        Me.cmbF.Text = "Выберите параметр"
        '
        'cmbБелок
        '
        Me.cmbБелок.BackColor = System.Drawing.Color.GhostWhite
        Me.cmbБелок.Font = New System.Drawing.Font("Microsoft YaHei UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.cmbБелок.ForeColor = System.Drawing.Color.Black
        Me.cmbБелок.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmbБелок.Items.AddRange(New Object() {"<=", ">="})
        Me.cmbБелок.Location = New System.Drawing.Point(163, 30)
        Me.cmbБелок.Name = "cmbБелок"
        Me.cmbБелок.Size = New System.Drawing.Size(58, 25)
        Me.cmbБелок.TabIndex = 10
        '
        'cmb2
        '
        Me.cmb2.BackColor = System.Drawing.Color.GhostWhite
        Me.cmb2.Font = New System.Drawing.Font("Microsoft YaHei UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.cmb2.ForeColor = System.Drawing.Color.Black
        Me.cmb2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmb2.Items.AddRange(New Object() {"<=", ">="})
        Me.cmb2.Location = New System.Drawing.Point(163, 77)
        Me.cmb2.Name = "cmb2"
        Me.cmb2.Size = New System.Drawing.Size(58, 25)
        Me.cmb2.TabIndex = 18
        '
        'cmb3
        '
        Me.cmb3.BackColor = System.Drawing.Color.GhostWhite
        Me.cmb3.Font = New System.Drawing.Font("Microsoft YaHei UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.cmb3.ForeColor = System.Drawing.Color.Black
        Me.cmb3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmb3.Items.AddRange(New Object() {"<=", ">="})
        Me.cmb3.Location = New System.Drawing.Point(163, 128)
        Me.cmb3.Name = "cmb3"
        Me.cmb3.Size = New System.Drawing.Size(58, 25)
        Me.cmb3.TabIndex = 19
        '
        'dgvRecipe
        '
        Me.dgvRecipe.AllowUserToResizeRows = False
        Me.dgvRecipe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecipe.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRecipe.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvRecipe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRecipe.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvRecipe.Location = New System.Drawing.Point(12, 335)
        Me.dgvRecipe.Name = "dgvRecipe"
        Me.dgvRecipe.RowHeadersVisible = False
        Me.dgvRecipe.Size = New System.Drawing.Size(613, 304)
        Me.dgvRecipe.TabIndex = 0
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.Location = New System.Drawing.Point(652, 335)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(363, 304)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.Visible = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.RadioButton2.FlatAppearance.BorderColor = System.Drawing.Color.LightCoral
        Me.RadioButton2.FlatAppearance.BorderSize = 3
        Me.RadioButton2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.RadioButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.RadioButton2.Location = New System.Drawing.Point(287, 65)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(146, 20)
        Me.RadioButton2.TabIndex = 7
        Me.RadioButton2.Text = "Маскимизировать"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.cmbF)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(471, 153)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Целевая функция"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(29, 31)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(410, 18)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Выберите целевую характеристику рецептуры:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(344, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 19)
        Me.Label1.TabIndex = 9
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.RadioButton1.Checked = True
        Me.RadioButton1.FlatAppearance.BorderColor = System.Drawing.Color.LightCoral
        Me.RadioButton1.FlatAppearance.BorderSize = 3
        Me.RadioButton1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.RadioButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.RadioButton1.Location = New System.Drawing.Point(287, 92)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(140, 20)
        Me.RadioButton1.TabIndex = 8
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Минимизировать"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbССВ)
        Me.GroupBox2.Controls.Add(Me.txtССВ)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.cmb3)
        Me.GroupBox2.Controls.Add(Me.cmb2)
        Me.GroupBox2.Controls.Add(Me.txtUgl)
        Me.GroupBox2.Controls.Add(Me.txtЖир)
        Me.GroupBox2.Controls.Add(Me.txtБелок)
        Me.GroupBox2.Controls.Add(Me.cmbБелок)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Location = New System.Drawing.Point(489, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(428, 312)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ограничения на общее содержание компонентов"
        '
        'cmbССВ
        '
        Me.cmbССВ.BackColor = System.Drawing.Color.GhostWhite
        Me.cmbССВ.Font = New System.Drawing.Font("Microsoft YaHei UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.cmbССВ.ForeColor = System.Drawing.Color.Black
        Me.cmbССВ.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmbССВ.Items.AddRange(New Object() {"<=", ">="})
        Me.cmbССВ.Location = New System.Drawing.Point(163, 200)
        Me.cmbССВ.Name = "cmbССВ"
        Me.cmbССВ.Size = New System.Drawing.Size(58, 25)
        Me.cmbССВ.TabIndex = 23
        '
        'txtССВ
        '
        Me.txtССВ.BackColor = System.Drawing.Color.GhostWhite
        Me.txtССВ.Font = New System.Drawing.Font("Monotype Corsiva", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtССВ.Location = New System.Drawing.Point(244, 200)
        Me.txtССВ.MaximumSize = New System.Drawing.Size(60, 60)
        Me.txtССВ.MinimumSize = New System.Drawing.Size(4, 4)
        Me.txtССВ.Name = "txtССВ"
        Me.txtССВ.Size = New System.Drawing.Size(60, 25)
        Me.txtССВ.TabIndex = 22
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(24, 179)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(245, 18)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Содержание сухих веществ"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(103, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(0, 19)
        Me.Label8.TabIndex = 20
        '
        'txtUgl
        '
        Me.txtUgl.BackColor = System.Drawing.Color.GhostWhite
        Me.txtUgl.Font = New System.Drawing.Font("Monotype Corsiva", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtUgl.Location = New System.Drawing.Point(244, 128)
        Me.txtUgl.MaximumSize = New System.Drawing.Size(60, 60)
        Me.txtUgl.MinimumSize = New System.Drawing.Size(4, 4)
        Me.txtUgl.Name = "txtUgl"
        Me.txtUgl.Size = New System.Drawing.Size(60, 25)
        Me.txtUgl.TabIndex = 17
        '
        'txtЖир
        '
        Me.txtЖир.BackColor = System.Drawing.Color.GhostWhite
        Me.txtЖир.Font = New System.Drawing.Font("Monotype Corsiva", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtЖир.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtЖир.Location = New System.Drawing.Point(244, 77)
        Me.txtЖир.MaximumSize = New System.Drawing.Size(60, 60)
        Me.txtЖир.MinimumSize = New System.Drawing.Size(4, 4)
        Me.txtЖир.Name = "txtЖир"
        Me.txtЖир.Size = New System.Drawing.Size(60, 25)
        Me.txtЖир.TabIndex = 16
        '
        'txtБелок
        '
        Me.txtБелок.BackColor = System.Drawing.Color.GhostWhite
        Me.txtБелок.Font = New System.Drawing.Font("Monotype Corsiva", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtБелок.Location = New System.Drawing.Point(244, 30)
        Me.txtБелок.MaximumSize = New System.Drawing.Size(60, 60)
        Me.txtБелок.MinimumSize = New System.Drawing.Size(4, 4)
        Me.txtБелок.Name = "txtБелок"
        Me.txtБелок.Size = New System.Drawing.Size(60, 25)
        Me.txtБелок.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(41, 129)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(90, 18)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Углеводы"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(41, 78)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 18)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Жиры"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(41, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 18)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Белки"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(103, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(0, 19)
        Me.Label2.TabIndex = 9
        '
        'btnOptim
        '
        Me.btnOptim.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnOptim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnOptim.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatAppearance.BorderSize = 0
        Me.btnOptim.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.btnOptim.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOptim.Font = New System.Drawing.Font("Microsoft YaHei UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnOptim.ForeColor = System.Drawing.Color.White
        Me.btnOptim.Image = CType(resources.GetObject("btnOptim.Image"), System.Drawing.Image)
        Me.btnOptim.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOptim.Location = New System.Drawing.Point(1073, 359)
        Me.btnOptim.Name = "btnOptim"
        Me.btnOptim.Size = New System.Drawing.Size(172, 36)
        Me.btnOptim.TabIndex = 13
        Me.btnOptim.Text = "Оптимизировать"
        Me.btnOptim.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOptim.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Location = New System.Drawing.Point(12, 171)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(471, 153)
        Me.GroupBox4.TabIndex = 21
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Общие показатели рецептуры"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label9.Location = New System.Drawing.Point(926, 212)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(322, 18)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Автор оптимизации: Кимсанбаев Карим"
        '
        'inputSettingsOptim
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(1438, 651)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(GroupBox3)
        Me.Controls.Add(Me.btnOptim)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.dgvRecipe)
        Me.Name = "inputSettingsOptim"
        Me.Text = "inputSettingsOptim"
        GroupBox3.ResumeLayout(False)
        CType(Me.dgvSettingMD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRecipe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvRecipe As DataGridView
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtБелок As TextBox
    Friend WithEvents txtUgl As TextBox
    Friend WithEvents txtЖир As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btnOptim As Button
    Friend WithEvents dgvSettingMD As DataGridView
    Friend WithEvents cmbF As ComboBox
    Friend WithEvents cmbБелок As ComboBox
    Friend WithEvents cmb2 As ComboBox
    Friend WithEvents cmb3 As ComboBox
    Friend WithEvents cmbССВ As ComboBox
    Friend WithEvents txtССВ As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label9 As Label
End Class
