﻿Public Class clIngredient
    'Код, название и цена ингредиента
    Public code As Long = 0
    Public name As String = "Безымянный"
    Public price As Double = 0
    'Структура состава ингредиента
    Public Structure structComponents
        Public вода#
        Public белок, жиры, углеводы As Double
        Public изолейц, лейц, валин, мет_цист, фен_тир, триптофан, лизин, треон As Double
        Public клечатка, зола As Double
    End Structure
    Public components As structComponents 'Переменная типа структура, через неё осуществляется доступ к компонентам ингредиента
    Public Содержание_сухих_веществ# = 0
    Public ЭЦпоумолчанию# = 0
    'Берёт код и по нему из БД заполняет объект этого класса
    Sub New(code As Long)
        Me.code = code
        name = GetNameIngredientByCode(code)
        components = GetComponentsByCode(code)
        price = GetPriceIngredientByCode(code)
    End Sub
    'Я не знаю нафиг это тут, но не трогай
    Public Sub fillЭЦandССВ()
        Содержание_сухих_веществ = GetССВ(code)
        ЭЦпоумолчанию = GetЭЦпоумолчанию(code)
    End Sub
    Sub New(name As String)
        Me.name = name
    End Sub
End Class
