﻿Module CreatePrototypeRecipes
    Private countPrototypes% = 0
    Private settings() As SettingsOptim.bondsSettings
    Private percentZam%()
    Private ControlRecipe As clRecipe

    Public Function LoadSettingsInModule(countPrototypes%, settings() As SettingsOptim.bondsSettings, percentZam%(), ControlRecipe As clRecipe) As clRecipe()
        CreatePrototypeRecipes.countPrototypes = countPrototypes
        CreatePrototypeRecipes.settings = settings
        CreatePrototypeRecipes.percentZam = percentZam
        CreatePrototypeRecipes.ControlRecipe = ControlRecipe

        Dim PrototypeRecipes(countPrototypes) As clRecipe

        For i% = 0 To countPrototypes - 1
            PrototypeRecipes(i) = CreatePrototype(percentZam(i))
        Next
        Calculate(PrototypeRecipes)

        'For i% = 0 To countPrototypes - 1
        '    Dim form As New ViewRecipeByClass
        '    form.ViewRecipe(PrototypeRecipes(i))
        'Next

        Return PrototypeRecipes
    End Function
    Private Function CreatePrototype(percentZam%) As clRecipe
        Dim recipe As New clRecipe
        recipe.CompositionRecipe = ControlRecipe.CompositionRecipe

        For i% = 0 To settings.Length - 1
            Dim indexReplaceable% = settings(i).replaceableIngredientIndex
            Dim ExpertIngredientMd# = 0
            Dim ReplaceableMd# = 0

            ExpertIngredientMd = recipe.CompositionRecipe(indexReplaceable).Md / 100 * percentZam
            recipe.AddIngredient(settings(i).expertIngredient, ExpertIngredientMd)

            ReplaceableMd = recipe.CompositionRecipe(indexReplaceable).Md - ExpertIngredientMd
            recipe.ChangeMD(indexReplaceable, ReplaceableMd)
        Next

        Return recipe
    End Function
    Private Sub Calculate(ByRef PrototypeRecipes() As clRecipe)
        For i% = 0 To countPrototypes - 1
            CalculateS_Components(PrototypeRecipes(i))
            CalculateЭЦ(PrototypeRecipes(i))
            CalculatePrice(PrototypeRecipes(i))
        Next
    End Sub
    Public Sub CalculateS_Components(ByRef PrototypeRecipe As clRecipe)
        PrototypeRecipe = S_Components(PrototypeRecipe)
    End Sub
    Public Sub CalculateЭЦ(ByRef PrototypeRecipe As clRecipe)
        PrototypeRecipe.ЭЦ_ккал = ЭЦ_ккал(PrototypeRecipe)
    End Sub
    Public Sub CalculatePrice(ByRef PrototypeRecipe As clRecipe)
        PrototypeRecipe.Price100g = Цена(PrototypeRecipe)
    End Sub
End Module
