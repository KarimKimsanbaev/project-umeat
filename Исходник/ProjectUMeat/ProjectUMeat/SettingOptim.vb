﻿Imports ProjectUMeat.frmSimplex
Public Class SettingOptim
    Dim codeRecipe% = 30051
    Dim recipe As clRecipe
    Private Sub SettingOptim_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        recipe = New clRecipe(codeRecipe)
        recipe.ЭЦandССВ()
        outputDGV()
        'cof()
    End Sub
    Public confines(3) As structConfines
    Dim arrMDot() As Double
    Dim arrMDdo() As Double
    Private Sub cof()
        confines(0).name = "Белки"
        confines(0).equality = "<="
        confines(0).valueCom = getbelok()
        confines(0).value = 1000


        confines(1).name = "Жиры"
        confines(1) = confines(1)
        confines(1).equality = "<="
        confines(1).valueCom = getjir()
        confines(1).value = 30

        confines(2).name = "Углеводы"
        confines(2) = confines(2)
        confines(2).equality = "<="
        confines(2).valueCom = getugl()
        confines(2).value = 70

        confines(3).name = "ССВ"
        confines(2) = confines(2)
        confines(3).equality = ">="
        confines(3).valueCom = getССВ()
        confines(3).value = 90

        Dim itprice#
        For i = 0 To recipe.CompositionRecipe.Length - 1
            itprice = itprice + recipe.CompositionRecipe(i).Ingredient.price * recipe.CompositionRecipe(i).Md
        Next
        TextBox1.Text = itprice

        dgv2.Columns.Add(1, 1)
        dgv2.Columns.Add(1, 1)
        dgv2.Columns.Add(1, 1)
        For i = 0 To confines.Length - 1
            dgv2.Rows.Add()
            dgv2.Rows(i).Cells(0).Value = confines(i).name
            dgv2.Rows(i).Cells(1).Value = confines(i).equality
            dgv2.Rows(i).Cells(2).Value = confines(i).value
        Next

        For i = 0 To recipe.CompositionRecipe.Length - 1
            DataGridView1.Rows.Add()
            DataGridView1.Rows(i).Cells(0).Value = recipe.CompositionRecipe(i).Ingredient.name
            DataGridView1.Rows(i).Cells(1).Value = recipe.CompositionRecipe(i).Ingredient.components.белок
            DataGridView1.Rows(i).Cells(2).Value = recipe.CompositionRecipe(i).Ingredient.components.жиры
            DataGridView1.Rows(i).Cells(3).Value = recipe.CompositionRecipe(i).Ingredient.components.углеводы
            DataGridView1.Rows(i).Cells(4).Value = recipe.CompositionRecipe(i).Ingredient.price
        Next
    End Sub
    Private Sub outputDGV()

        ReDim arrMDot(recipe.CompositionRecipe.Length - 1)

        arrMDot(0) = 40
        arrMDot(1) = 5
        arrMDot(2) = 0
        arrMDot(3) = 0
        arrMDot(4) = 0
        arrMDot(5) = 0
        arrMDot(6) = 0

        ReDim arrMDdo(recipe.CompositionRecipe.Length - 1)

        arrMDdo(0) = 90
        arrMDdo(1) = 10
        arrMDdo(2) = 10
        arrMDdo(3) = 12
        arrMDdo(4) = 40
        arrMDdo(5) = 40
        arrMDdo(6) = 20

        arrMDot = arrMDot.Select(Function(x) x / 100).ToArray
        arrMDdo = arrMDdo.Select(Function(x) x / 100).ToArray


        arrMDdo = arrMDdo

        Dim confMD(recipe.CompositionRecipe.Length - 1) As structConfines

        For i = 0 To recipe.CompositionRecipe.Length - 1
            Dim g As structConfines
            g.name = recipe.CompositionRecipe(i).Ingredient.name
            g.value = arrMDot(i)
            g.equality = ">="

            Dim arr#(recipe.CompositionRecipe.Length - 1)
            arr.SetValue(1, i)

            g.valueCom = arr
            confMD(i) = g
        Next


        Dim confMDdo(recipe.CompositionRecipe.Length - 1) As structConfines

        For i = 0 To recipe.CompositionRecipe.Length - 1
            Dim g As structConfines
            g.name = recipe.CompositionRecipe(i).Ingredient.name
            g.value = arrMDdo(i)
            g.equality = "<="

            Dim arr#(recipe.CompositionRecipe.Length - 1)
            arr.SetValue(1, i)

            g.valueCom = arr
            confMDdo(i) = g
        Next

        'Dim x As structConfines = confines(0)
        confines(0).name = "Белки"
        confines(0).equality = "<="
        confines(0).valueCom = getbelok()
        confines(0).value = 1000


        confines(1).name = "Жиры"
        confines(1) = confines(1)
        confines(1).equality = "<="
        confines(1).valueCom = getjir()
        confines(1).value = 30

        confines(2).name = "Углеводы"
        confines(2) = confines(2)
        confines(2).equality = "<="
        confines(2).valueCom = getugl()
        confines(2).value = 70


        confines(3).name = "ССВ"
        confines(2) = confines(2)
        confines(3).equality = ">="
        confines(3).valueCom = getССВ()
        confines(3).value = 90

        'Array.Resize(confines, confMD.Length - 1)
        'confines.Concat(confMD)
        Array.Resize(confines, confines.Length + confMD.Length + confMDdo.Length - 1)
        Array.ConstrainedCopy(confMD, 0, confines, 3, confMD.Length)
        'Array.Resize(confines, confines.Length + confMD.Length + confMDdo.Length)
        Array.ConstrainedCopy(confMDdo, 0, confines, 3 + confMD.Length, confMDdo.Length)
        confines = confines

        'confines(3).name = "Вода"
        'confines(2) = confines(2)
        'confines(3).equality = "<="
        'confines(3).valueCom = getwater()
        'confines(3).value = 18

        Dim itprice#
        For i = 0 To recipe.CompositionRecipe.Length - 1
            itprice = itprice + recipe.CompositionRecipe(i).Ingredient.price * recipe.CompositionRecipe(i).Md
        Next
        TextBox1.Text = itprice

        dgv2.Columns.Add(1, 1)
        dgv2.Columns.Add(1, 1)
        dgv2.Columns.Add(1, 1)
        For i = 0 To confines.Length - 1
            dgv2.Rows.Add()
            dgv2.Rows(i).Cells(0).Value = confines(i).name
            dgv2.Rows(i).Cells(1).Value = confines(i).equality
            dgv2.Rows(i).Cells(2).Value = confines(i).value
        Next

        For i = 0 To recipe.CompositionRecipe.Length - 1
            DataGridView1.Rows.Add()
            DataGridView1.Rows(i).Cells(0).Value = recipe.CompositionRecipe(i).Ingredient.name
            DataGridView1.Rows(i).Cells(1).Value = recipe.CompositionRecipe(i).Ingredient.components.белок
            DataGridView1.Rows(i).Cells(2).Value = recipe.CompositionRecipe(i).Ingredient.components.жиры
            DataGridView1.Rows(i).Cells(3).Value = recipe.CompositionRecipe(i).Ingredient.components.углеводы
            DataGridView1.Rows(i).Cells(4).Value = recipe.CompositionRecipe(i).Ingredient.price
        Next
        'DataGridView1.Columns.Item(2).Visible = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click




        confines = confines

        Dim obj#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To recipe.CompositionRecipe.Length - 1
            obj(i) = recipe.CompositionRecipe(i).Ingredient.price
        Next

        obj = obj

        frmSimplex.Show()
        frmSimplex.start(obj, confines, recipe)
    End Sub
    'Заменить на функцию по удобнее
    Function getbelok() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.components.белок
        Next
        Return arr
    End Function
    Function getjir() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.components.жиры
        Next
        Return arr
    End Function
    Function getugl() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.components.углеводы
        Next
        Return arr
    End Function
    Function getССВ() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.Содержание_сухих_веществ
        Next
        Return arr
    End Function
End Class