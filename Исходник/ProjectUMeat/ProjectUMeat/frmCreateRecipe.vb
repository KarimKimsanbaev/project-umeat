﻿'Imports System.Data.SqlClient
'Public Class frmCreateRecipe
'    Public codeRecipe As Long = 0
'    Private Sub frmEditorRecipes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
'        IngredientsTableAdapter.Fill(ProductsDBDataSet.Ingredients)
'        Dim nameRecipe As String = ""
'        If codeRecipe <> 0 Then
'            nameRecipe = Me.RecipesTableAdapter1.GetNameRecipe(codeRecipe)
'            Dim tableView As ProductsDBDataSet.IngDataTable
'            tableView = Me.IngTableAdapter1.FillDataRecipeByCode(codeRecipe)
'            For i2% = 0 To tableView.Rows.Count - 1
'                Dim row2 As DataRow
'                row2 = tableView.Rows.Item(i2)
'                Dim codeIng As Long = 0
'                codeIng = row2.Item("Код ингредиента")
'                Dim md As Double = 0
'                md = row2.Item("Массовая доля")
'                Dim row As DataRow
'                row = Me.IngredientsTableAdapter.GetIngByCode(codeIng).Item(0)

'                If AddRow(row, table) Then
'                    Return
'                End If

'                'Назначение источника данных
'                If IsNothing(dgvRecipe.DataSource) Then
'                    dgvRecipe.DataSource = table
'                End If

'                md = md / 1000
'                dgvRecipe.Rows(i2).Cells("Массовая_доля").Value = md

'                'Форматирование dgvRecipe
'                dgvRecipe.Columns.Item("Код").Visible = False
'                If dgvRecipe.Columns("Массовая_доля").Visible = False Then
'                    dgvRecipe.Columns("Массовая_доля").DisplayIndex = 2
'                    dgvRecipe.Columns("Массовая_доля").Visible = True

'                    For i% = 0 To dgvRecipe.ColumnCount - 1
'                        If dgvRecipe.Columns(i).Name <> "Массовая_доля" Then
'                            dgvRecipe.Columns(i).ReadOnly = True
'                        End If
'                    Next
'                    dgvRecipe.Columns("Массовая_доля").ReadOnly = False
'                    dgvRecipe.Columns(0).Frozen = True
'                End If
'            Next
'        End If
'        Label1.Text = nameRecipe
'    End Sub

'    Dim table As New DataTable("Table")
'    Private Sub InsertDgv(row As DataRow)
'        If AddRow(row, table) Then
'            Return
'        End If

'        'Назначение источника данных
'        If IsNothing(dgvRecipe.DataSource) Then
'            dgvRecipe.DataSource = table
'        End If
'        'Форматирование dgvRecipe
'        dgvRecipe.Columns.Item("Код").Visible = False
'        If dgvRecipe.Columns("Массовая_доля").Visible = False Then
'            dgvRecipe.Columns("Массовая_доля").DisplayIndex = 2
'            dgvRecipe.Columns("Массовая_доля").Visible = True

'            For i% = 0 To dgvRecipe.ColumnCount - 1
'                If dgvRecipe.Columns(i).Name <> "Массовая_доля" Then
'                    dgvRecipe.Columns(i).ReadOnly = True
'                End If
'            Next
'            dgvRecipe.Columns("Массовая_доля").ReadOnly = False
'            dgvRecipe.Columns(0).Frozen = True
'        End If
'    End Sub
'    Private Sub btnСохранить_Click(sender As Object, e As EventArgs)
'        Dim name As String = ""
'        name = InputBox("Введите имя рецепта", "Имя рецепта")

'        If name = "" Then
'            Return
'        End If

'        Dim description As String = ""
'        description = InputBox("Введите комментарий к рецепту")

'        Dim conn As New SqlConnection
'        conn.ConnectionString = "Data Source=HOME-PC;Initial Catalog=ProductsDB;Integrated Security=SSPI;User ID=sa;Password=;"
'        conn.Open()

'        Dim comm As New SqlCommand
'        comm.CommandText =
'            "INSERT INTO Recipes 
'            VALUES('" & name & description & "')
'            SELECT SCOPE_IDENTITY()"
'        comm.Connection = conn

'        Dim code As Integer
'        code = comm.ExecuteScalar()

'        Dim id_Ing As Integer = 0

'        For i% = 0 To dgvRecipe.RowCount - 2 '1, если пользователю нельзя добавлять строки в DataGridView
'            id_Ing = dgvRecipe.Rows(i).Cells(0).Value
'            comm.CommandText =
'            "INSERT INTO Ing
'                VALUES(" & code & ", " & id_Ing & ", 20)"
'            comm.ExecuteNonQuery()
'        Next

'        MsgBox("Рецептура сохранена")
'    End Sub

'    Private Sub Button1_Click(sender As Object, e As EventArgs)
'        Me.IngredientsBindingSource.EndEdit()
'        Me.TableAdapterManager.UpdateAll(Me.ProductsDBDataSet)
'    End Sub

'    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
'        Dim row As DataRow
'        row = НазваниеListBox.SelectedItem.Row 'Row - для корректного совмещения типов
'        InsertDgv(row)
'    End Sub
'    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
'        'Проверки
'        If dgvRecipe.RowCount = 0 Then
'            MsgBox("Нет ни одного ингредиента в рецептуре, добавьте что - нибудь")
'            Return
'        End If
'        Dim err As Boolean = False
'        For i% = 0 To dgvRecipe.RowCount - 1
'            Try
'                If dgvRecipe.Rows(i).Cells("Массовая_доля").Value = 0 Then
'                    MessageBox.Show("Массовая доля в строке " & i + 1 & " пуста или является 0", "Ошибка Массовой доли", MessageBoxButtons.OK, MessageBoxIcon.Error)
'                    dgvRecipe.Rows(i).Cells("Массовая_доля").Style.BackColor = Color.Yellow
'                    err = True
'                Else
'                    dgvRecipe.Rows(i).Cells("Массовая_доля").Style.BackColor = Color.White
'                End If
'            Catch ex As Exception
'                'MessageBox.Show("Сообщение для разработчика: Не найден столбец", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)
'                MessageBox.Show("Массовая доля в строке " & i + 1 & " пуста или имеет недопустимые символы", "Ошибка Массовой доли", MessageBoxButtons.OK, MessageBoxIcon.Error)
'                dgvRecipe.Rows(i).Cells("Массовая_доля").Style.BackColor = Color.Yellow
'                err = True
'            End Try
'        Next
'        If err Then
'            Return
'        End If
'        'Существует ли такой рецепт
'        If codeRecipe <> 0 Then
'            Dim result As Integer = 0
'            result = MessageBox.Show("Такой рецепт уже существует, создать как новый? (Если нажать 'Нет', изменения буду сохранены в старом рецепте)", "Такой рецепт уже существует", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
'            If result = 6 Then
'                codeRecipe = 0
'            End If

'            If result = 2 Then
'                MsgBox("Рецепт не сохранён")
'                Return
'            End If
'        End If
'        'Настройка подключения
'        Dim conn As New SqlConnection
'        conn.ConnectionString = GetConnectionString()
'        conn.Open()
'        Dim comm As New SqlCommand
'        comm.Connection = conn
'        If codeRecipe = 0 Then
'            'Ввод имени и описания
'            Dim name As String = ""
'            name = InputBox("Введите имя рецепта", "Имя рецепта")

'            If name = "" Then
'                MessageBox.Show("Имя не может быть пустым, рецепт не сохранён", "Ошибка в имени рецепта!", MessageBoxButtons.OK, MessageBoxIcon.Error)
'                Return
'            End If

'            Dim description As String = ""
'            description = InputBox("Введите комментарий к рецепту")
'            'Создание в БД новой рецептуры
'            comm.CommandText =
'                "INSERT INTO Recipes 
'            VALUES('" & name & "','" & description & "')
'            SELECT SCOPE_IDENTITY()"
'        End If

'        'Создания нового рецепта или очистка содержимого старого рецепта
'        If codeRecipe = 0 Then
'            codeRecipe = comm.ExecuteScalar()
'            If codeRecipe = 0 Then
'                MessageBox.Show("Сообщение для Разработчка: Произошла ошибка при сохранении рецепта в БД", "Ошибка Запроса!", MessageBoxButtons.OK, MessageBoxIcon.Error)
'                Return
'            End If
'            'MsgBox("Код нового рецепта: " & codeRecipe)
'        Else
'            comm.CommandText =
'                    "DELETE Ing
'                WHERE [Код рецепта] = " & codeRecipe
'            comm.ExecuteNonQuery()
'            'MsgBox("Найден существующий рецепт, все ингредиенты в нём удалены")
'        End If
'        'Запись в БД содержимого рецепта
'        Dim codeIng As Long = 0
'        Dim md As Double = 0
'        Dim ingName As String = ""
'        Try
'            For i% = 0 To dgvRecipe.RowCount - 1
'                ingName = dgvRecipe.Rows(i).Cells("Название").Value
'                codeIng = Me.IngredientsTableAdapter.GetCode(ingName)
'                Debug.WriteLine("Получен код ингредиента с название " & ingName & ", его код " & codeIng)

'                md = dgvRecipe.Rows(i).Cells("Массовая_доля").Value

'                Debug.WriteLine("Получена массовая доля " & ingName & ": " & md)
'                Dim m As String = md
'                m = m * 1000
'                If m.Contains(",") Then
'                    m = m.Replace(",", ".")
'                End If
'                comm.CommandText =
'                "INSERT INTO Ing
'                VALUES(" & codeRecipe & ", " & codeIng & ", " & m & ")"
'                'MsgBox(md)
'                comm.ExecuteNonQuery()
'                Debug.WriteLine("Запись ингредиента " & ingName & " завершена")
'            Next
'        Catch ex As ArgumentException
'            MsgBox("Сообщение для разработчка: Не найден столбец 'Название' или 'Массовая_ доля'!")
'        End Try
'        conn.Close()
'        MsgBox("Рецепт сохранён")
'        'Кимсанбаев Карим красачик!!!
'    End Sub

'    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
'        If codeRecipe <> 0 Then
'            Dim result% = 0
'            result = MessageBox.Show("Все измнения не будут сохранены, вы уверены что хотите продолжить?", "Сообщение", MessageBoxButtons.YesNo)
'            If result = 6 Then
'                frmViewAnalytic.code = codeRecipe
'                frmViewAnalytic.Show()
'            End If
'        Else
'            MsgBox("Этот рецепт не сохранён, сперва сохраните рецепт")
'        End If
'    End Sub

'    Private Sub НазваниеListBox_DoubleClick(sender As Object, e As EventArgs) Handles НазваниеListBox.DoubleClick
'        Dim row As DataRow
'        row = НазваниеListBox.SelectedItem.Row
'        InsertDgv(row)
'    End Sub
'End Class