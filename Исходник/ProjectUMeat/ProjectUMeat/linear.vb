﻿Imports ProjectUMeat.frmSimplex
Public Class linear
    Public arr#()
    Private length%

    Sub New(confines() As structConfines)
        ReDim arr(confines.Length - 1)
        For i = 0 To confines.Length - 1
            If confines(i).equality = ">=" Then
                arr(i) = confines(i).value * -1
                confines(i).valueCom = confines(i).valueCom.Select(Function(x) -x).ToArray
                confines(i).value *= -1
            Else
                arr(i) = confines(i).value
            End If
        Next
        length = arr.Length
        outputB()
    End Sub
    Public Function isOptim() As Boolean
        Return arr.All(predicate:=Function(x) x >= 0)
    End Function
    Public Function indexAbsMinusUpper() As Integer
        If frmSimplex.iteration = 6 Then
            'Stop
        End If
        Dim a#() = Array.FindAll(arr, Function(x) x < 0)

        If a.Length = 0 Then
            MsgBox("Нет массива отрицательных элементов в ограничениях. Условие выше почему - то не сработало")
            Stop
        End If
        Dim r# = a.Max(Function(x) Math.Abs(x))
        Return Array.FindIndex(arr, match:=Function(x) x = -r)
    End Function
    Public Function divArrayConfines(perColumnMem#()) As Double()
        Dim arrResult#(perColumnMem.Length - 1)
        perColumnMem.CopyTo(arrResult, 0)
        For i = 0 To perColumnMem.Length - 1
            If perColumnMem(i) <> 0 Then
                arrResult(i) = arr(i) / perColumnMem(i)
            End If
        Next
        Return arrResult
    End Function
    Public Sub outputB()
        frmSimplex.outputB(arr)
    End Sub
    Public Function GetLength() As Integer
        Return length
    End Function
    Public Sub Recount(xrs As cell, perColumn#())
        For i = 0 To length - 1
            If i <> xrs.perRow Then
                arr(i) = (xrs.value * arr(i) - perColumn(i) * arr(xrs.perRow)) / xrs.value
            End If
        Next
        arr(xrs.perRow) = arr(xrs.perRow) / xrs.value
    End Sub
End Class
