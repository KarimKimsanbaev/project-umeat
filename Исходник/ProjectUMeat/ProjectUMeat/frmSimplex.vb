﻿Imports ProjectUMeat
Imports ProjectUMeat.OutputOptim

Public Class frmSimplex
    Public Sub Log(Optional str As String = "")
        txtLog.Text = txtLog.Text & str & Chr(13)
    End Sub
    Public Sub outputF(f() As Double)
        Try
            Log("Вывод целвой функции в dgv")
            For i = 0 To f.Length - 1
                If dgvF.Columns.Count < f.Length Then
                    dgvF.Columns.Add(i, i)
                End If
                If dgvF.Rows.Count < 1 Then
                    dgvF.Rows.Add()
                End If
                dgvF.Rows(0).Cells(i).Value = f(i)
            Next
        Catch
            MsgBox("Ошибка вывода целевой функции в dgv")
        End Try
    End Sub
    Public Sub outputB(b() As Double)
        Try
            Log("Вывод ограничений в dgv")
            If dgvB.Columns.Count < 1 Then
                dgvB.Columns.Add(1, 1)
            End If
            For i = 0 To b.Length - 1
                If dgvB.Rows.Count < b.Length Then
                    dgvB.Rows.Add()
                End If
                dgvB.Rows(i).Cells(0).Value = b(i)
            Next
        Catch
            MsgBox("Ошибка вывода ограничений в dgv")
        End Try
    End Sub
    Public Sub outputX()
        Dim x(BazisOpt.x.Length - 1) As Double
        For i = 0 To BazisOpt.x.Length - 1
            If BazisOpt.x(i).inBazis Then
                Dim ind% = BazisOpt.x(i).index
                Log("x" & i & " = " & b.arr(ind) & " " & BazisOpt.x(i).inBazis)
                x(i) = b.arr(ind)
            Else
                Dim ind% = BazisOpt.x(i).index
                Log("x" & i & " = 0")
                x(i) = 0
            End If
        Next
    End Sub
    Public Structure structBazis
        Dim index%
        Public inBazis As Boolean
    End Structure
    Public Sub outputallbazis()

        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Columns.Add(1, 1)
        dgvNotBazis.Rows.Add(1, 1)

        dgvBazis.Columns.Add(0, 0)
    End Sub
    Public Sub outputBazis(Bazis() As Integer)
        Try
            Log("Вывод базисных переменных в dgv")
            dgvBazis.Columns.Add(1, 1)
            For i = 0 To Bazis.Length - 1
                dgvBazis.Rows.Add()
                dgvBazis.Rows(i).Cells(0).Value = Bazis(i)
            Next
        Catch
            MsgBox("Ошибка вывода базисных переменных в dgv")
        End Try
    End Sub
    Public Sub outputNotBazis(NotBazis() As Integer)
        Try
            Log("Вывод не базисных переменных в dgv")
            For i = 0 To NotBazis.Length - 1
                dgvNotBazis.Columns.Add(i, i)
                dgvNotBazis.Rows.Add()
                dgvNotBazis.Rows(0).Cells(i).Value = NotBazis(i)
            Next
        Catch
            MsgBox("Ошибка вывода не базисных переменных в dgv")
        End Try
    End Sub
    Public Sub outputA(A()() As Double, colCnt%, rowCnt%)
        Log("Вывод таблицы коэффицентов в dgv")
        Try
            For i = 0 To colCnt - 1
                If dgvA.ColumnCount < i + 1 Then
                    dgvA.Columns.Add(i, i)
                End If
                For j = 0 To rowCnt - 1
                    dgvA.Rows.Add()
                    dgvA.Rows(j).Cells(i).Value = A(j)(i)
                Next
            Next
        Catch
            MsgBox("Ошибка вывода таблицы коэффицентов в dgv")
        End Try
    End Sub
    Private Sub outputValueTargetF(val As Double)
        ValueTargetF.Text = val
    End Sub
    Private Sub markPerColumn(index As Integer)
        dgvA.SelectionMode = SelectionMode.None
        dgvA.Columns(index).DefaultCellStyle.BackColor = Color.Yellow
    End Sub
    Private Sub markXrs(indRow%, indCol%)
        dgvA.Rows(indRow).Cells(indCol).Style.BackColor = Color.Green
    End Sub
    Private Sub markPerRow(index As Integer)
        dgvA.Rows(index).DefaultCellStyle.BackColor = Color.Orange
    End Sub
    Public f As ObjectiveFunction
    Public b As linear
    Public Bazis() As Integer
    Public NotBazis() As Integer
    Public A As matrixA
    Public obj(1) As Double
    Public Structure structConfines
        Dim name As String
        Dim valueCom#()
        Dim equality As String
        Dim value As Double
    End Structure

    Dim Confines() As structConfines
    'Dim bt As linear
    Private Sub setConfines()
        ReDim Confines(2)
        With Confines(0)
            .name = "Белок"
            .valueCom = New Double(1) {1, -2}
            .equality = "<="
            .value = 1
        End With
        With Confines(1)
            .name = "Жиры"
            .valueCom = New Double(1) {-2, 1}
            .equality = "<="
            .value = 2
        End With
        With Confines(2)
            .name = "Углеводы"
            .valueCom = New Double(1) {2, 1}
            .equality = "<="
            .value = 6
        End With
    End Sub
    Private recipe As clRecipe
    Public Sub isNotSolve()
        'OutputOptim.ShowDialog()
        OutputOptim.output(b.arr, recipe, Confines)
        n = True
    End Sub
    Dim n As Boolean
    Public Sub start(obj#(), confines() As structConfines, recipe As clRecipe)
        Me.obj = obj
        Me.Confines = confines

        Me.recipe = recipe
        Me.Show()
        CanonicalForm()
        'Exit Sub
        'Log()
        'If ft.isOptim() Then
        '    Log("ЗАДАЧА РЕШЕНА!")
        '    Return
        'End If
        'outputallbazis()
        'Exit Sub

        Do While iteration <> -1
            dgvA.Update()
            dgvB.Update()
            dgvF.Update()
            If n = True Then
                Exit Sub
            End If
            'Stop

            iteration = iteration + 1
            Log("Началась итерация " & iteration)

            For i = 0 To BazisOpt.x.Length - 1
                If BazisOpt.x(i).inBazis Then
                    Dim ind% = BazisOpt.x(i).index
                    Log("x" & i & " = " & b.arr(ind) & " " & BazisOpt.x(i).inBazis & " индекс = " & BazisOpt.x(i).index)
                Else
                    Dim ind% = BazisOpt.x(i).index
                    Log("x" & i & " = " & f.arr(ind) & " " & BazisOpt.x(i).inBazis & " индекс = " & BazisOpt.x(i).index)
                End If
            Next

            Dim isSolve As Boolean = False
            isSolve = FindingXrs()
            If isSolve Then
                Solve()
                Exit Sub
            Else
                Log("Задача ещё не решена")
            End If
            'Exit Sub
            'Exit Sub
            buildNewTable()
            outputX()

            dgvA.Update()
            dgvB.Update()
            dgvF.Update()
            If n = True Then
                Exit Sub
            End If
            'Exit Sub
            'Stop
            'n = True
            'Exit Sub
            'isSolve = False
            'isSolve = FindingXrs()
            'If isSolve Then
            '    Solve()
            '    Exit Sub
            'Else
            '    Log("Задача ещё не решена")
            'End If
            ''Exit Sub
            'buildNewTable()
            'outputX()
        Loop
    End Sub
    Public Sub Solve()
        'BazisOpt.x(1).inBazis = True
        Dim x(BazisOpt.x.Length - 1) As Double
        For i = 0 To BazisOpt.x.Length - 1
            If BazisOpt.x(i).inBazis Then
                Dim ind% = BazisOpt.x(i).index
                Log("x" & i & " = " & b.arr(ind) & " " & BazisOpt.x(i).inBazis)
                x(i) = b.arr(ind)
            Else
                Dim ind% = BazisOpt.x(i).index
                Log("x" & i & " = 0")
                x(i) = 0
            End If
        Next
        OutputOptim.output(x, recipe, Confines)
    End Sub
    Private Sub Simplex_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        'obj(0) = 3 : obj(1) = 1

        'setConfines() 'Функция, которой не будет в оригинале


    End Sub
    Private BazisOpt As BazisOption
    Private Sub CanonicalForm()
        Log("Запускаю построение канонической формы")
        Log("Привожу целевую функцию к канонической форме")
        f = New ObjectiveFunction(obj)
        f.outputF()
        Log("Длина целевой функции " & f.GetLength())


        Log("Привожу ограничения к канонической форме")
        b = New linear(Confines)


        Dim NotBazisLength% = f.GetLength()
        Log("Строю шаблон матрицы коэффицентов. Ширина матрицы " & NotBazisLength)
        A = New matrixA(NotBazisLength)
        Log("Добавляю в матрицу ограничения")
        A.buildMatrix(Confines)
        A.output()

        Log("Настраиваю базис")
        BazisOpt = New BazisOption(NotBazisLength)


        Log("Построение канонической формы завершено. Перехожу к расчёту.")
    End Sub
    Structure cell
        Dim value#, perRow%, perColumn%
    End Structure
    'Dim perColumn%, perRow%
    Dim xrs As cell
    Private isMaximize As Boolean = True
    Private Function FindingXrs() As Integer
        Log()
        Log("Запускаю выбор разрешающего элемента")
        Log("Определяю метод нахождения разрешающего элемента")
        Dim ind%
        If b.isOptim() Then
            Log("Среди ограничений нет отрицательных значений. Метод нахождения выбран обычным")
            Log("Выбираю разрешающий столбец")
            ind = selectPerColumnFromF()
            If ind = -800 Then
                Return 1
            End If
            Log("Наибольшим по модулю отрицательным элементов выбран " & f.arr(ind) & " под индексом " & ind)
            xrs.perColumn = ind
            markPerColumn(xrs.perColumn)
            Log("Разрешающим столбцом выбран " & xrs.perColumn)

            Log("Выбираю разрешающую строку")

            xrs.perRow = selectPerRow(xrs.perColumn)
            markPerRow(xrs.perRow)

            Log("Разрешающий строкой выбран " & xrs.perRow)
            xrs.value = GetXrs()
            Log("Разрешающим элементом выбрано значение " & xrs.value)
        Else
            Log("В ограничениях найден отрицательный коэффицент. Метод нахождения выбран двойственным")
            Log("Выбираю разрешающий столбец")
            Log("Изымаю наибольший по модулю отрицательный элемент среди ограничений")
            ind = selectPerRowFromB()
            Log("Наибольшим по модулю отрицательным элементов выбран " & b.arr(ind) & " под индексом " & ind)
            Log("Индекс " & ind & " выбран в качестве разрешающей строки")
            xrs.perRow = ind
            markPerRow(ind)
            Log("Выбираю разрешающий столбец")
            xrs.perColumn = selectPerColumn(ind)
            markPerColumn(xrs.perColumn)
            xrs.value = GetXrs()
            Log("Разрешающим элементом выбрано значение " & xrs.value)
        End If
    End Function
    Private Function selectPerColumnFromF() As Integer
        Return f.indexAbsMinusUpper()
    End Function
    Private Function selectPerRowFromB() As Integer
        Return b.indexAbsMinusUpper()
    End Function
    Public iteration% = 0

    Private Function selectPerRow(perColumn%) As Integer
        Dim nonMinusMem#() = A.GetNonMinusMemColumn(perColumn)

        If nonMinusMem.Length = 0 Then

        End If

        Dim deltaArr#()

        If Not Array.Exists(nonMinusMem, Function(x) x > 0) Then
            isNotSolve()
        End If
        deltaArr = b.divArrayConfines(nonMinusMem)
        Dim min#
        Dim indPerRow%
        Try
            min = Array.FindAll(deltaArr, Function(x) x > 0).Min
            'min = deltaArr.Min()
            indPerRow% = Array.FindIndex(deltaArr, Function(x) x = min)
        Catch ex As Exception
            nonMinusMem = nonMinusMem
            MsgBox("Задача не имеет решения!")
            'Application.Exit()
            Log("ЗАДАЧА НЕ ИМЕЕТ РЕШЕНИЯ!")
            isNotSolve()
            Return Nothing
        End Try
        If min = 0 Then
            Log("ЗАДАЧА НЕ ИМЕЕТ РЕШЕНИЯ!")
            Return Nothing
        End If
        Log("Разрешающая строка по индексом " & indPerRow)
        Return indPerRow

    End Function
    Private Function GetXrs()
        Return A.GetValue(xrs.perRow, xrs.perColumn)
    End Function
    Private Function selectPerColumn(perRow%) As Integer
        Dim nonMinusMem#() = A.GetNonMinusMemRow(perRow)

        If nonMinusMem.Length = 0 Then

        End If

        Dim deltaArr#()

        If Not Array.Exists(nonMinusMem, Function(x) x < 0) Then
            isNotSolve()
        End If
        deltaArr = b.divArrayConfines(nonMinusMem)
        Dim min#
        Dim indPerColumn%
        Try
            min = Array.FindAll(deltaArr, Function(x) Math.Abs(x)).Min
            'min = deltaArr.Min()
            indPerColumn% = Array.FindIndex(deltaArr, Function(x) x = min)
        Catch ex As Exception
            nonMinusMem = nonMinusMem
            MsgBox("Задача не имеет решения!")
            'Application.Exit()
            Log("ЗАДАЧА НЕ ИМЕЕТ РЕШЕНИЯ!")
            isNotSolve()
            Return Nothing
        End Try
        If min = 0 Then
            Log("ЗАДАЧА НЕ ИМЕЕТ РЕШЕНИЯ!")
            Return Nothing
        End If
        Log("Минимальным от деления элементом выбран " & f.arr(indPerColumn) & " Разрешающая строка по индексом " & indPerColumn)
        Return indPerColumn

    End Function
    Private Function buildNewTable()
        Log()
        Log("Запускаю построение нового опорного решения")
        BazisOpt.Swap(xrs.perRow, xrs.perColumn)
        'outputNotBazis(BazisOpt.x.Select(Function(x) x.index).ToArray)
        outputallbazis()
        Dim arrPerColumn#() = A.GetColumn(xrs.perColumn)
        Dim arrPerRow#() = A.GetRow(xrs.perRow)


        'Log("Меняю местами базис")
        'BazisOpt.Swap(xrs.perColumn, xrs.perRow)
        'Log("Базисы под индексами " & xrs.perColumn & " и " & xrs.perRow & " меняются местами")
        'BazisOpt.output()
        A.RecountFreeMem(xrs)
        A.output()
        b.Recount(xrs, arrPerColumn)
        b.outputB()
        f.Recount(xrs, arrPerRow)
        f.outputF()

        A.RecountPerColumn(xrs)
        A.RecountPerRow(xrs)


        A.RecountXrs(xrs)
        A.output()
    End Function

    Private Sub buildNewTable2()

        Dim newA(NotBazis.Length, Bazis.Length) As Double
        Dim newb(Bazis.Length) As Double
        Dim newf(NotBazis.Length) As Double

        Try
            Log("Перевожу разрешающий элемент в новую таблицу")
            'Log("Старый разрешающий элемент = " & xrs & " новый = " & 1 / xrs)
        Catch ex As ArgumentNullException
            MsgBox("Во время перевода разрешающего элемента произошло деление на 0, проверьте параметры построения")
        End Try
        ' newA(perRow, perColumn) = 1 / xrs

        Log("Перевожу разрешающую строку в новую таблицу")
        For i = 0 To NotBazis.Length - 1
            'If i = perColumn Then
            'Continue For
            'End If
            ' Log("Перерасчёт " & matrixbA(perRow, i) & " в " & matrixbA(perRow, i) / xrs)
            'newA(perRow, i) = matrixbA(perRow, i) / xrs
        Next

        'newb(perRow) = b(perRow) / xrs
        'outputB(b)

        Log()
        'Log("Перевожу разрешающий столбец в новую таблицу")
        'For i = 0 To Bazis.Length - 1
        '    If i = perRow Then
        '        Continue For
        '    End If
        '    'Log("Перерасчёт " & matrixbA(i, perColumn) & " в " & matrixbA(i, perColumn) / xrs * -1)
        '    'newA(i, perColumn) = matrixbA(i, perColumn) / xrs * -1
        'Next
        'newf(perColumn) = f(perColumn) / xrs * -1
        'outputF(f)

        Log()
        Log("Перевожу все остальные элементы в новую таблицу")
        'Dim xij#, xrj#, xis#

        Log()
        'For i = 0 To Bazis.Length - 1
        '    For j = 0 To NotBazis.Length - 1
        '        If i = perRow Or j = perColumn Then
        '            Continue For
        '        End If
        '        'xij = matrixbA(i, j)
        '        Log("Новым перерасчётным элементов выбрано " & xij)
        '        'xrj = matrixbA(perRow, j)
        '        'xis = matrixbA(i, perColumn)
        '        Log("Для элемента " & xij & " выбрано xrj = " & xrj)
        '        Log(" и xis = " & xis)
        '        'newA(i, j) = (xrs * xij - xrj * xis) / xrs
        '        'Log("Новым значением для элемента " & xij & " будет " & matrixbA(i, j))
        '    Next
        'Next
        'outputA(newA, NotBazis.Length, Bazis.Length)

        'Log()
        'Log("Пересчитываем ограничения")
        'For i = 0 To Bazis.Length - 1
        '    If i <> perRow Then
        '        'Log("i = " & i & " b(i) = " & b(i))
        '        ' b(i) = (xrs * b(i) - b(perRow) * matrixbA(i, perColumn)) / xrs
        '        'Log("Новый b(i) = " & b(i))
        '    End If
        'Next
        '' b(perRow) = b(perRow) * xrs
        ''outputB(b)
    End Sub

    'Private Function isNotSolve() As Boolean
    '    Log("Проверяю задачу на разрешимость")
    '    For i = 0 To Bazis.Length - 1
    '        'Во время тестирования проверить это место!
    '        If matrixbA(perColumn, i) >= 0 Then
    '            Return False
    '        End If
    '    Next
    '    Return True
    'End Function
End Class