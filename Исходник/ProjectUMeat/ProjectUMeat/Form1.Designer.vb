﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ОписаниеLabel = New System.Windows.Forms.Label()
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.RecipesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RecipesTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.RecipesTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.RecipesDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ОписаниеTextBox = New System.Windows.Forms.TextBox()
        Me.IngBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ViewReceptBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ViewReceptTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.ViewReceptTableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.IngTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngTableAdapter()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RecipesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RecipesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Label1.Location = New System.Drawing.Point(425, 197)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(153, 20)
        Label1.TabIndex = 16
        Label1.Text = "Состав рецептуры:"
        '
        'ОписаниеLabel
        '
        Me.ОписаниеLabel.AutoSize = True
        Me.ОписаниеLabel.Location = New System.Drawing.Point(426, 9)
        Me.ОписаниеLabel.Name = "ОписаниеLabel"
        Me.ОписаниеLabel.Size = New System.Drawing.Size(117, 13)
        Me.ОписаниеLabel.TabIndex = 9
        Me.ОписаниеLabel.Text = "Описание рецептуры:"
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RecipesBindingSource
        '
        Me.RecipesBindingSource.DataMember = "Recipes"
        Me.RecipesBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'RecipesTableAdapter
        '
        Me.RecipesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IngredientsTableAdapter = Nothing
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Me.RecipesTableAdapter
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'RecipesDataGridView
        '
        Me.RecipesDataGridView.AllowUserToAddRows = False
        Me.RecipesDataGridView.AllowUserToDeleteRows = False
        Me.RecipesDataGridView.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.RecipesDataGridView.AutoGenerateColumns = False
        Me.RecipesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.RecipesDataGridView.BackgroundColor = System.Drawing.SystemColors.Window
        Me.RecipesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RecipesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.RecipesDataGridView.DataSource = Me.RecipesBindingSource
        Me.RecipesDataGridView.Location = New System.Drawing.Point(8, 26)
        Me.RecipesDataGridView.MultiSelect = False
        Me.RecipesDataGridView.Name = "RecipesDataGridView"
        Me.RecipesDataGridView.RowHeadersVisible = False
        Me.RecipesDataGridView.Size = New System.Drawing.Size(411, 465)
        Me.RecipesDataGridView.TabIndex = 9
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Код"
        Me.DataGridViewTextBoxColumn1.FillWeight = 30.45685!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Код"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Рецепт"
        Me.DataGridViewTextBoxColumn2.FillWeight = 169.5432!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Рецепт"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'ОписаниеTextBox
        '
        Me.ОписаниеTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngBindingSource, "Массовая доля", True))
        Me.ОписаниеTextBox.Location = New System.Drawing.Point(429, 26)
        Me.ОписаниеTextBox.Multiline = True
        Me.ОписаниеTextBox.Name = "ОписаниеTextBox"
        Me.ОписаниеTextBox.Size = New System.Drawing.Size(263, 168)
        Me.ОписаниеTextBox.TabIndex = 10
        '
        'IngBindingSource
        '
        Me.IngBindingSource.DataMember = "Ing"
        Me.IngBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ViewReceptBindingSource
        '
        Me.ViewReceptBindingSource.DataMember = "ViewRecept"
        Me.ViewReceptBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ViewReceptTableAdapter
        '
        Me.ViewReceptTableAdapter.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(704, 26)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 46)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Новый рецепт"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(785, 26)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 46)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "Изменить состав"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(866, 26)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 46)
        Me.Button5.TabIndex = 14
        Me.Button5.Text = "Удалить рецепт"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'IngTableAdapter
        '
        Me.IngTableAdapter.ClearBeforeFill = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Format = "0.###"
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(429, 221)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(515, 256)
        Me.DataGridView1.TabIndex = 17
        '
        'Button1
        '
        Me.Button1.Image = Global.ProjectUMeat.My.Resources.Resources.playIcon1
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.Location = New System.Drawing.Point(704, 139)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(132, 55)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "Оптимизировать"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.ProjectUMeat.My.Resources.Resources.calculateIcon
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.Location = New System.Drawing.Point(704, 78)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(198, 55)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "Расчёт" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " по Липатова Н. Н. Рогова"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(230, 18)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Авторы ПО: Кимсанбаев Карим"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(956, 482)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ОписаниеLabel)
        Me.Controls.Add(Me.ОписаниеTextBox)
        Me.Controls.Add(Me.RecipesDataGridView)
        Me.Name = "Form1"
        Me.Text = "Просмотр рецептов"
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RecipesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RecipesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents RecipesBindingSource As BindingSource
    Friend WithEvents RecipesTableAdapter As ProductsDBDataSetTableAdapters.RecipesTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents RecipesDataGridView As DataGridView
    Friend WithEvents ОписаниеTextBox As TextBox
    Friend WithEvents ViewReceptBindingSource As BindingSource
    Friend WithEvents ViewReceptTableAdapter As ProductsDBDataSetTableAdapters.ViewReceptTableAdapter
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents IngBindingSource As BindingSource
    Friend WithEvents IngTableAdapter As ProductsDBDataSetTableAdapters.IngTableAdapter
    Friend WithEvents Button1 As Button
    Friend WithEvents ОписаниеLabel As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents Label2 As Label
End Class
