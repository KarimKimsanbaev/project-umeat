﻿Public Class SettingsOptim
    Private countPrototypes% = 0

    Public ControlRecipe As clRecipe

    Dim percentZam%(0)
    Public Structure bondsSettings
        Dim expertIngredient As clIngredient
        Dim replaceableIngredientIndex%
    End Structure
    Private settings(0) As bondsSettings
    Private Sub SettingsOptim_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvViewSettings.Rows.Add() 'Вставка строки заголовка

        If IsNothing(ControlRecipe) Then
            ControlRecipe = New clRecipe(20)
        End If
        ViewControlRecipe()

        DelimiterFormating()

        'Добавление опытного ингредиента
        'Dim ing As New clIngredient(11)
        'AddExpertIngredient(ing)
        'Dim ing2 As New clIngredient(10)
        'AddExpertIngredient(ing2)5


        'Создание опытных образцов
        'AddPrototypeModel(3)
        'AddPrototypeModel(5)
        'AddPrototypeModel(8)
    End Sub
    Private Sub ViewControlRecipe()
        'Вставка рецепта из clRecipe
        For i% = 0 To ControlRecipe.CompositionRecipe.Length - 1
            dgvViewSettings.Rows.Add()
            dgvViewSettings.Rows(i + 1).Cells("Название_компонента").Value = ControlRecipe.CompositionRecipe(i).Ingredient.name
            dgvViewSettings.Rows(i + 1).Cells("МД_Контрольного_образца").Value = ControlRecipe.CompositionRecipe(i).Md
        Next
    End Sub
    Private Sub ViewExpertIngredient(ingredient As clIngredient)
        Dim index% = dgvViewSettings.Rows.Add()
        dgvViewSettings.Rows(index).Cells("Название_компонента").Value = ingredient.name
    End Sub
    Private Sub AddExpertIngredient(ingredient As clIngredient)
        PushBack(ingredient)
        ViewExpertIngredient(ingredient)
        ViewToZamDGV()
    End Sub
    Private Sub ViewToZamDGV()
        Dim row As New DataGridViewRow
        row.CreateCells(dgvZamSettings)
        row.Cells(0).Value = settings(settings.Length - 1).expertIngredient.name

        Dim col2 As DataGridViewComboBoxCell

        col2 = row.Cells(2)

        For j% = 0 To ControlRecipe.CompositionRecipe.Length - 1
            col2.Items.Add(ControlRecipe.CompositionRecipe(j).Ingredient.name)
        Next
        col2.Value = ""

        dgvZamSettings.Rows.Add(row)
    End Sub
    Private Sub AddPrototypeModel(Optional percent% = 0)
        If IsNothing(settings(0).expertIngredient) Then
            MsgBox("Сперва выберите опытные компоненты")
            Return
        End If
        If percent = 0 Then
            Dim st As String
            st = InputBox("Введите процент замещения")
            If st = "" Then
                MsgBox("Введите процент замещения")
                Return
            Else
                percent = Convert.ToInt64(st)
            End If
        End If

        countPrototypes = countPrototypes + 1
        'Добавление нового столбца для опытного
        Dim index%
        Dim col As New DataGridViewColumn(dgvViewSettings.Columns(0).CellTemplate)
        col.Name = "Опытный " & countPrototypes '2 - следующий индекс опытного образца (может использоваться в будущем для создания массива)
        index = dgvViewSettings.Columns.Add(col)
        col.DefaultCellStyle.Alignment = ContentAlignment.MiddleCenter

        'Добавление процента замещения для этого опытного образца
        dgvViewSettings.Rows(0).Cells(index).Value = percent
        dgvViewSettings.Rows(0).DefaultCellStyle.Alignment = ContentAlignment.MiddleCenter
        dgvViewSettings.Rows(0).DefaultCellStyle.Format = "#'%'"

        PushBack(percent)

        CalculateMdPrototype()
    End Sub
    Private Sub CalculateMdPrototype()
        For cntZam% = 1 To countPrototypes
            Dim percentZam% = dgvViewSettings.Rows(0).Cells(cntZam + 1).Value
            Dim md As Double = 0
            For i% = 0 To settings.Length - 1
                Dim indexZam% = settings(i).replaceableIngredientIndex
                md = ControlRecipe.CompositionRecipe(indexZam).Md / 100 * percentZam
                dgvViewSettings.Rows(ControlRecipe.CompositionRecipe.Length + 2 + i).Cells(cntZam + 1).Value = md

                dgvViewSettings.Rows(indexZam + 1).Cells(cntZam + 1).Value = ControlRecipe.CompositionRecipe(indexZam).Md - md
            Next


            For i% = 0 To ControlRecipe.CompositionRecipe.Length - 1
                Dim b As Boolean = False
                For j% = 0 To settings.Length - 1
                    If i = settings(j).replaceableIngredientIndex Then
                        b = True
                    End If
                Next

                If b = False Then
                    dgvViewSettings.Rows(i + 1).Cells(cntZam + 1).Value = ControlRecipe.CompositionRecipe(i).Md
                End If
            Next
        Next
    End Sub
    Private Sub DelimiterFormating()
        'Вставка разделителя между ингредиентам и в рецепте и опытными ингредиентами
        Dim row As New DataGridViewRow
        row.CreateCells(dgvViewSettings)
        row.Cells(0).Value = "Опытные компоненты:"
        row.DefaultCellStyle.BackColor = Color.Yellow
        dgvViewSettings.Rows.Add(row)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        AddPrototypeModel(0)
    End Sub
    Private Sub PushBack(value%)
        If percentZam(0) = 0 Then
            'MsgBox("Новые проценты")
            percentZam(0) = value
        Else
            Array.Resize(percentZam, percentZam.Length + 1)
            percentZam(percentZam.Length - 1) = value
            'MsgBox("Очередные проценты")
        End If
    End Sub
    Private Sub PushBack(ingredient As clIngredient)
        If IsNothing(settings(0).expertIngredient) Then
            settings(0).expertIngredient = ingredient
        Else
            Array.Resize(settings, settings.Length + 1)
            settings(settings.Length - 1).expertIngredient = ingredient
        End If
    End Sub
    Private Sub dgvZamSettings_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvZamSettings.CellEndEdit
        Dim comboCell As DataGridViewComboBoxCell
        comboCell = dgvZamSettings.Rows(e.RowIndex).Cells(2)
        If comboCell.Value <> "" Then
            settings(e.RowIndex).replaceableIngredientIndex = comboCell.Items.IndexOf(comboCell.Value)
            CalculateMdPrototype()
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim code As Long
        Dim form As New SelectorIngredients
        form.ShowDialog()
        code = form.selectedIngredient

        Dim ing As New clIngredient(code)

        AddExpertIngredient(ing)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim PrototypeRecipes() As clRecipe
        PrototypeRecipes = LoadSettingsInModule(countPrototypes, settings, percentZam, ControlRecipe)
        Dim row0 As New DataGridViewRow
        row0.CreateCells(dgvViewSettings)
        row0.DefaultCellStyle.BackColor = Color.YellowGreen
        dgvViewSettings.Rows.Add(row0)
        Dim row As New DataGridViewRow
        Dim row2 As New DataGridViewRow

        row.CreateCells(dgvViewSettings)
        row2.CreateCells(dgvViewSettings)

        row.Cells(0).Value = "Энергетическая ценность"
        Dim rec As clRecipe = ControlRecipe
        CalculateS_Components(rec)
        CalculateЭЦ(rec)
        CalculatePrice(rec)
        row.Cells(1).Value = rec.ЭЦ_ккал

        For i% = 0 To countPrototypes - 1
            row.Cells(i + 2).Value = PrototypeRecipes(i).ЭЦ_ккал
        Next

        row2.Cells(0).Value = "Цена за 100 г"
        row2.Cells(1).Value = rec.Price100g
        row2.DefaultCellStyle.Format = "#.##"

        For i% = 0 To countPrototypes - 1
            row2.Cells(i + 2).Value = PrototypeRecipes(i).Price100g
        Next

        dgvViewSettings.Rows.Add(row)
        dgvViewSettings.Rows.Add(row2)
    End Sub
End Class